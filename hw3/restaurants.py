import sys
import os.path
import threading
import cherrypy
import os
import math
import json
from collections import OrderedDict
import mysql.connector as MySQL
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
import logging
from config import conf
import getpass

from restaurantid import RestaurantId

class Restaurants(object):
    exposed = True
    dirname = os.path.abspath( os.path.dirname(__file__) )
    env = Environment(loader = FileSystemLoader(os.path.join(dirname, 'templates')))

    def __init__(self, db_user="test", db_passwd="mypass", db_host="127.0.0.1", db_name="restaurant"):
        self.rid = RestaurantId(db_user, db_passwd, db_host, db_name, self.env)
        self.myd = dict()
        self.xtra = dict()

        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host
        self.mean_radius=6371000. # mean radius in meters    

    def _cp_dispatch(self, vpath):
        print "Restaurant._cp_dispatch with vpath: %s \n" % vpath
        len_vpath = len(vpath)
        if len_vpath >= 1: # /restaurants/{restId}
            cherrypy.request.params['restId'] = vpath.pop(0)
            if len_vpath == 1:
                return self.rid
        if len_vpath >= 2: # /restaurants/{restId}/menus
            vpath.pop(0)
            if len_vpath == 2:
                return self.rid.rest_menus
        if len_vpath >= 3: # /restaurants/{restId}/menus/{menuId}
            cherrypy.request.params['menuId'] = vpath.pop(0)
            if len_vpath == 3:
                return self.rid.rest_menus.mid
        if len_vpath >= 4: # /restaurants/{restId}/menus/{menuId}/sections
            vpath.pop(0)
            if len_vpath == 4:
                return self.rid.rest_menus.mid
        if len_vpath >= 5:# /restaurants/{restId}/menus/{menuId}/sections/{sectionId}
            cherrypy.request.params['sectionId'] = vpath.pop(0)
            if len_vpath == 5:
                return self.rid.rest_menus.mid.sectionsid
        if len_vpath >= 6: #/restaurants/{restId}/menus/{menuId}/sections/{sectionId}/subsections
            vpath.pop(0)
            if len_vpath == 6:
                return self.rid.rest_menus.mid.sectionsid
        if len_vpath >= 7: #/restaurants/{restId}/menus/{menuId}/sections/{sectionId}/subsections/{subsectionId}
            cherrypy.request.params['subsectionId'] = vpath.pop(0)
            if len_vpath == 7:
                return self.rid.rest_menus.mid.sectionsid.suid
        if len_vpath >= 8: #/restaurants/{restId}/menus/{menuId}/sections/{sectionId}/subsections/{subsectionId}/menu_items/
            vpath.pop(0)
            if len_vpath == 8:
                return self.rid.rest_menus.mid.sectionsid.suid
        if len_vpath >= 9:#/restaurants/{restId}/menus/{menuId}/sections/{sectionId}/subsections/{subsectionId}/menu_items/{menu_itemId}
            cherrypy.request.params["menu_itemId"] = vpath.pop(0)
            if len_vpath == 9:
                return self.rid.rest_menus.mid.sectionsid.suid.miid

        return vpath

    def getDataFromDB(self):
        try:
            conn = MySQL.connect(
                    user=self.db_user,
                    password = self.db_passwd,
                    host=self.db_host,
                    database=self.db_name)
            cur = conn.cursor()
            q = "select restId, name, lat, lng, street_address, locality, region, website_url from restaurants"
            cur.execute(q)
        except Error as e:
            logging.error(e)
            raise

        self.data = []
        for (restId, name, lat, lng, street_address, locality, region, website_url) in cur:
            self.data.append({
                    'name': name,
                    'lat': str(lat),
                    'long': str(lng),
                    'city': locality,
                    'state': region,
                    'url': website_url,
                    'href': 'restaurants/' + str(restId)
                    })
            self.myd[restId] = (float(lat), float(lng))
            self.xtra[restId] = (street_address, locality, region, website_url, name)


    def haversine(self, p0, p1):
        metersToFeet = 3.28084
        lat1,long1=p0
        lat2,long2=p1
        dlat=(lat2-lat1)*(math.pi/180.)
        dlong=(long2-long1)*(math.pi/180.)
        a = math.sin(dlat/2.)**2 + math.sin(dlong/2)**2 * math.cos(lat1) * math.cos(lat2)
        c = 2. * math.atan2(math.sqrt(a),math.sqrt(1.-a))
        return self.mean_radius * c * metersToFeet

    def GET(self):
        ''' Get list of restaurants '''
        lat = 41.698318
        lng = -86.236218

        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        self.getDataFromDB()
        self.sd = dict()
        for key, value in self.myd.iteritems():
            dist = self.haversine((lat, lng), value)
            self.sd[ key ] = round(dist, 0)

        result = OrderedDict(sorted(self.sd.items(), key=lambda t: t[1]))
        if output_format == 'text/html':
            return self.env.get_template('restaurants.html').render(
                    restaurants = result,
                    info = self.xtra
                )
        else:
            return json.dumps(self.data, encoding='utf-8')
        

    def POST(self, **kwargs):
        ''' Add a new restaurant '''
        result= "POST /restaurants     ...     Restaurants.POST\n"
        result+= "POST /restaurants body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data; restID should not be included
        # Insert restaurant
        # Prepare response
        return result

        result = "POST /restaurants"

    def OPTIONS(self):
        ''' Allows GET, POST, OPTIONS '''
        return "<p>/restaurants/ allows GET, POST, and OPTIONS</p>"

class StaticAssets(object):
    pass

if __name__ == '__main__':
    passwd = getpass.getpass("input database password: ")
    cherrypy.tree.mount(Restaurants(db_user = 'root', db_passwd = passwd), '/restaurants',{
            '/': { 'request.dispatch': cherrypy.dispatch.MethodDispatcher() }
        })
    cherrypy.tree.mount(StaticAssets(), '/', {
            '/': {
                'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
                },
            '/static': {
                'tools.staticdir.on': True,
                'tools.staticdir.dir': 'static'
                }
        })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    sys.stdout = sys.stderr
    application = cherrypy.Application(Restaurants(), None, conf)
