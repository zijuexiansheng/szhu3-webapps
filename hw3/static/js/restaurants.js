$(document).ready(function(){
    $(document).on('click','a', function(event){
        if($(this).hasClass("menu"))
            handle_menu($(this), event);
        else if($(this).hasClass("section"))
            handle_section($(this), event);
        else if($(this).hasClass("subsection"))
        {
            console.log("call handle_subsection");
            handle_subsection($(this), event);
        }
        else if($(this).hasClass("menu_item"))
        {
            console.log("call handle_menu_item");
            handle_menu_item($(this), event);
        }
        else
            console.log("simply click");
    })
    
    $(document).on('submit', 'form', function(event){
        event.preventDefault();
        var url = $(this).attr("action");
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data
        }).done(function(data){
            $.globalMessenger().post({
                message: data,
                type: 'info',
                showCloseButton: true
            });
        }).fail(function(){
            $.globalMessenger().post({
                message: "Oooops! Something Wrong!!!",
                type: 'error',
                showCloseButton: true
            });
        });
    });
});


