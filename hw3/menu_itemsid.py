import cherrypy
import mysql.connector as MySQL
from mysql.connector import Error
import logging
import json

#from subsectionsid import SubSectionsId

class MenuItemsId(object):
    ''' Handles resource /restuarant/{restId}/menus/{menuId}/sections/{sectionId}'''

    exposed = True
    def __init__(self, db_user, db_passwd, db_host, db_name, env):
        #self.suid = SubSectionsId(db_user, db_passwd, db_host, db_name, env)
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host
        self.env = env

    def getOptionGroupsFromDB(self, menu_itemId):
        try:
            conn = MySQL.connect(
                    user = self.db_user,
                    password = self.db_passwd,
                    host = self.db_host,
                    database = self.db_name)
            cur = conn.cursor()

            stmt = "select option_groupId, menu_itemId, option_type, option_group_text from option_group where menu_itemId = %(menu_itemId)s"
            cur.execute(stmt, {'menu_itemId': menu_itemId})
            return cur.fetchall()
        except Error as e:
            logging.error(e)
            raise

    def getOptionsFromDB(self, option_groupId):
        try:
            conn = MySQL.connect(
                    user = self.db_user,
                    password = self.db_passwd,
                    host = self.db_host,
                    database = self.db_name)
            cur = conn.cursor()

            stmt = "select optionId, option_groupId, name, price from options where option_groupId = %(option_groupId)s"
            cur.execute(stmt, {'option_groupId': option_groupId})
            return cur.fetchall()
        except Error as e:
            logging.error(e)
            raise
        

    def GET(self, restId, menuId, sectionId, subsectionId, menu_itemId):
        ''' return info on restaurants/{restId}/menus/{menuId} '''
        print ">>> MenuItemsIdId GET"
        output_format = cherrypy.lib.cptools.accept(['application/json'])
        option_groups = self.getOptionGroupsFromDB(menu_itemId)
        print "option_groups =", option_groups
        options = []
        for one_group in option_groups:
            options.append(self.getOptionsFromDB(one_group[0]))
        return json.dumps( [option_groups,
                            options,
                            restId, 
                            menuId,
                            sectionId,
                            subsectionId], 
                        encoding="utf-8" )

    def PUT(self, restId, menuId, sectionId, subsectionId, menu_itemId, **kargs):
        return "lol"

    def DELETE(self, restId, menuId, sectionId, subsectionId, menu_itemId):
        return "lol"

    def OPTIONS(self, restId, menuId, sectionId, subsectionId, menu_itemId):
        return "lol"
        




