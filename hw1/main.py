import cherrypy
import sys
import mysql.connector
import os
from jinja2 import Environment, FileSystemLoader

class StaticFilePath:
    def __init__(self, path = 'static'):
        self.static_url = path

class ExampleApp(object):
    dirname = os.path.dirname(__file__)
    env = Environment(loader = FileSystemLoader(os.path.join(dirname, 'templates')))
    @cherrypy.expose
    def index(self):
        from collections import OrderedDict
        res = OrderedDict()
        res['Subway'] = ('Lafortune Student Ctr, Notre Dame', 427.0)
        res['O\'Rourke\'s Public House'] = ('1044 Angela Blvd., South Bend', 632.0)
        res['The Mark Dine & Tap'] = ('1234 Eddy St., South Bend', 730.0)
        res['Linebacker Lounge'] = ('1631 S. Bend Ave., South Bend', 952.0)
        res['Starbucks'] = ('217 South Dining Hall, Notre Dame', 1230.0)
        res['Mandarin House'] = ('2104 Edison Rd., South Bend', 1434.0)
        res['Legends of Notre Dame'] = ('100 Legends, Notre Dame', 1468)
        res['Club 23'] = ('744 N. Notre Dame Ave., South Bend', 1481.0)
        res['Sunny Italy Cafe'] = ('601 N. Niles Ave., South Bend', 1777.0)
        res['Burger King'] = ('University Of Notre Dame, Notre Dame', 1836.0)
        res['ciao\'s'] = ('501 North Niles Ave., South Bend', 1845.0)
        res['Macri\'s Italian Bakery'] = ('214 North Niles Ave., South Bend', 2308.0)
        res['The Emporium'] = ('121 S. Niles Ave., South Bend', 2549.0)
        res['Barnaby\'s'] = ('713 E. Jefferson Blvd., South Bend', 2583.0)
        res['Sorin\'s'] = ('1 Notre Dame Ave., South Bend', 6487.0)
        res['Rohr\'s'] = ('1 Notre Dame, South Bend', 6487.0)

        res = sorted(res.iteritems(), key = lambda x: x[1][1]) # sort by distance
        #res = sorted(res.iteritems(), key = lambda x: x[0]) # sort by name
        html = self.env.get_template("index.html")
        return html.render(restaurants = res,
                            static_url = static_url.static_url)

    @cherrypy.expose
    def showdb(self):
        cnx = mysql.connector.connect(user='test', password='mypass',
                              host='127.0.0.1',
                              database='testdb')
        cursor = cnx.cursor()
        query = ("SELECT firstname,lastname,email FROM Invitations")
        cursor.execute(query)
        info = str()
        print cursor
        for (firstname, lastname, email) in cursor:
           info = info + "Full Name:" + lastname + firstname + "Email: "+email
        return info

if __name__ == "__main__":
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
            },
        '/static':{
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'static'
            }
        }
    static_url = StaticFilePath('static/')
    cherrypy.quickstart(ExampleApp(), '/', conf)
else:
    curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
    conf = {
        '/static':{
            'tools.staticdir.on': True,
            'tools.staticdir.root': curdir,
            'tools.staticdir.dir': 'static'
        }
    }
    static_url = StaticFilePath('/api/static/')
    application = cherrypy.Application(ExampleApp(), None)
    application.merge(conf)

