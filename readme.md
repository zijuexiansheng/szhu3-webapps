# hw2
## `price` type
* There are weired strings for the `price` entry in the json file like:
    * `"price": "2.00 each"`
    * `"price": "Market"`
    * `"price": "+ 0.50"`
* `price` in table `menu_item` and `options` are of type `varchar(20)`
* The typecasting of `price` will be done by python

## UML schema
* `restaurant-UML.pdf` or `restaurant-UML.vsdx`

## hyperlink of restaurant
* The hyperlink of the restaurant is the `website_url` in the json file

