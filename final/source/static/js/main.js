(function(){
var editor = new Behave({
	textarea: document.getElementById("wmd-input"),
	softTabs: true,
	tabSize: 4,
	autoIndent: true
});
var converter = new Markdown.Converter();
Markdown.Extra.init(converter, {highlighter: "prettify"});

MathJax.Hub.Config({
	extensions: ["tex2jax.js"],
	jax: ["input/TeX", "output/HTML-CSS"],
	tex2jax: {
		inlineMath: [ ['$', '$'], ["\\(", "\\)"] ],
		displayMath: [ ['$$', '$$'], ["\\[", "\\]"] ],
		processEscapes: true
		},
	"HTML-CSS": {availableFonts: ["TeX"]}
});

var toggle = 1;
function preview_update()
{
	var wmd_input = document.getElementById("wmd-input");
	var preview = document.getElementById("preview");
	if(toggle)
	{
		preview.innerHTML = converter.makeHtml(wmd_input.value);
		prettyPrint();
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, "preview"]);
		
		wmd_input.style.display = 'none';
		preview.style.display = '';
	}
	else
	{
		wmd_input.style.display = '';
		preview.style.display = 'none';
	}

	toggle ^= 1;
}

function hotkey(e)
{
	var evt = e || window.event;
	var letter = evt.keyCode;
	if((letter == 109 || letter == 77) && (evt.ctrlKey) && (evt.altKey))
	{
		console.log("update");
		preview_update();
	}
}
document.onkeydown = hotkey;
})();
