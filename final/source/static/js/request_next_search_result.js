sm_aid = null;

function getCookie(name)
{
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

function showErrorMsg(msg)
{
    $.globalMessenger().post({
                message: msg,
                type: "error",
                showCloseButton: "true"
            });
}

function run_paged_scroll(page_url, search_option, search_content)
{
    $("html, body").animate({scrollTop: "5px"}, 10);

    req_href = $("#partial_result div:last-child").find("h2").find("a").attr("href");
    req_t1 = req_href.indexOf("-");
    req_t1 = req_href.slice(req_t1 + 1);
    sm_aid = parseInt(req_t1.slice(0, req_t1.indexOf("-")));
    console.log("sm_aid = " + sm_aid);

    $(window).paged_scroll({
        handleScroll: function(page, container, doneCallback){
                var this_obj = this;
                console.log("request data: " + page + ", from bottom: " + this.triggerFromBottom);
                $.ajax({
                    type: "POST",
                    url: page_url,
                    data: {"sm_aid": sm_aid, 
                            "search_option": search_option,
                            "search": search_content,
                            "_xsrf": getCookie("_xsrf")},
                    dataType: 'json'
                }).done(function(data){
                    if(data["code"] == 2000)
                    {
                        $("#partial_result").append(data["message"]);
                        sm_aid = data['sm_aid'];
                    }
                    else if(data["code"] == 2001)
                    {// last one
                        this_obj.pagesToScroll = -1;
                        $("#partial_result").append($("<div/>"));
                    }
                    else
                    {
                        showErrorMsg("Error: " + data["code"] + ", Message: " + data["message"]);
                        $("#partial_result").append($("<div/>"));
                    }
                }).fail(function(){
                    showErrorMsg("Oooops, something wrong!!!");
                    $("#partial_result").append($("<div/>"));
                });
                return true;
            },
        triggerFromBottom: '10%',
        loading: {
                html: '<div class="paged-scroll-loading"><img alt="loading" src="/static/img/main2.gif"/></div>'
            },
        loader: '<div class="loader"></div>',
        targetElement: $('#partial_result'),
        //debug: true,
    });
}

