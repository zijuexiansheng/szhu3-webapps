$(function()
{
    function isValidEmail(email)
    {
        var re = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
        return re.test(email);
    }

    function isValidPassword(pwd) 
    {
        // at least one number, one lowercase, one uppercase letter, one special symbol
        // at least nine characters
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[?!_#%^&@+-]).{9,}/;
        return re.test(pwd);
    }

    function isValidPhone(phone) {
        // phone must be all digits
        var re = /^\d*$/;
        return re.test(phone);
    }

    function showErrorMsg(msg)
    {
        $.globalMessenger().post({
                message: msg,
                type: "error",
                showCloseButton: "true"
            });
    }

    $('#register-form').on('submit', function(event) { // form id
        event.preventDefault();
        var username = $('#username').val();
        var email = $('#email').val();
        var pwd = $('#password').val();
        var confirm_pwd = $("#confirm-password").val();

        if(username == "")
        {
            showErrorMsg('All fields must be filled. "Username" is empty');
            return;
        }

        if(email == "")
        {
            showErrorMsg('All fields must be filled. "Email" is empty');
            return;
        }
        
        if(pwd == "")
        {
            showErrorMsg('All fields must be filled. "Password" is empty');
            return;
        }

        if(pwd != confirm_pwd)
        {
            showErrorMsg('"password" and "Confirm password" don\'t match ');
            return;
        }

        if(!isValidEmail(email))
        {
            showErrorMsg('Email must be in the correct format');
            return;
        }

        if(!isValidPassword(pwd))
        {
            showErrorMsg('Password has to be 9 or more characters, and contain at least 1 upper case, 1 lower case, 1 number, and 1 symbol.');
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/loonblog/register',
            data: $(this).serialize(),
            dataType: 'json'
        }).done(function(data){
            if(data['code'] == 1000)
                window.location.href = data['message'];
            else
                showErrorMsg(data["code"] + ": " + data["message"]);
        }).fail(function(){
            showErrorMsg("Ooops, something wrong!!!");
        })
    });
});


