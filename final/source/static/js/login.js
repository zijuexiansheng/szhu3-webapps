$(function(){
    function showErrorMsg(msg)
    {
        $.globalMessenger().post({
                            message: msg,
                            type: "error",
                            showCloseButton: "true"
                            });
    }            
    $("#login-form").on('submit', function(event){
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '/loonblog/login',
            data: $(this).serialize(),
            dataType: 'json'
        }).done(function(data){
            if(data['code'] == 1000)
                window.location.href = data['message'];
            else
                showErrorMsg(data['code'] + ": " + data["message"]);
            
        }).fail(function(){
            showErrorMsg("Ooops, something wrong!!!");
        });
    });
});

