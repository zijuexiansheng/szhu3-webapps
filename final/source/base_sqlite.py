import os
import sqlite3
import sys

class SQLite:
    def __init__(self):
        dirname = os.path.dirname(__file__)
        sys.stderr.write(os.path.join(dirname, "LoonBlog.db"))
        self.conn = sqlite3.connect(os.path.join(dirname, "LoonBlog.db"))
        self.cur = self.conn.cursor()
        self.cur.execute("PRAGMA foreign_keys=ON")

    def get_cursor(self):
        return self.cur

    def commit(self):
        self.conn.commit()

    def rollback(self):
        self.conn.rollback()

    def close_db(self):
        self.cur.close()
        self.conn.close()
