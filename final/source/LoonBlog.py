import sqlite3
from passlib.hash import sha256_crypt as sha256
import time
import os
import re
import logging

import tornado.ioloop
import tornado.web
import tornado.escape
import tornado.wsgi
from tornado.options import options, define

from base_secure import *
from base_sqlite import *
from register import *
from login_out import *
from visitor_op import *
from auth_op import *

#define("port", default=8888, help="run on the given port", type=int)
dirname = os.path.dirname(__file__)

class HomepageHandler(SecureHandler):
    def get(self):
        if not self.current_user:
            self.render("homepage.html")
        else:
            self.redirect("/loonblog/articlelist")

settings = {
    "debug": True,
    "template_path": os.path.join(dirname, 'templates'),
    "static_path": os.path.join(dirname, 'static'),
    "cookie_secret": "LoonnooL",
    "login_url": "/loonblog",
    #"xsrf_cookies": True,
}

tornado_app = tornado.web.Application([
    (r"/loonblog", HomepageHandler),
    (r"/loonblog/login", LoginHandler),
    (r"/loonblog/logout", LogoutHandler),
    (r"/loonblog/register", RegisterHandler),
    (r"/loonblog/articlelist", ArticlelistHandler),
    (r"/loonblog/newarticle", NewArticleHandler),
    (r"/loonblog/editprofile", EditProfileHandler),
    (r"/loonblog/deleteuser", DeleteUserHandler),
    (r"/loonblog/view-([0-9]+)-article", ViewArticleHandler),
    (r"/loonblog/edit-([0-9]+)-article", EditArticleHandler),
    (r"/loonblog/delete-([0-9]+)-article", DeleteArticleHandler),
    (r"/loonblog/search", SearchHandler),
    (r"/loonblog/searchall", SearchAllHandler),
    (r"/loonblog/viewall-([0-9]+)-article", ViewAllArtivleHandler),
    (r"/loonblog/pictures", PicturesHandler),
    ], **settings)

if __name__ == '__main__':
    port = 8000
    options.log_file_prefix = './LoonBlog@%s.log' % port
    tornado.options.parse_command_line()
    tornado_app.listen(port)

    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        print "Exit"
else:
    options.log_file_prefix = os.path.join(dirname, 'LoonBlog.log')
    tornado.options.parse_command_line()
    application = tornado.wsgi.WSGIAdapter(tornado_app)
