import tornado.web
import tornado.escape
import sqlite3

from passlib.apps import custom_app_context as pwd_context
from pyvalidate import validate, ValidationException

from base_sqlite import *
from base_secure import *
from apiutil import *

class LoginHandler(SecureHandler):
    def post(self):
        email = self.get_argument("email", None)
        password = self.get_argument("password", None)

        if not email:
            return self.write(errorJSON(code=9005, message=str("Email is not filled")))
        if not password:
            return self.write(errorJSON(code=9005, message=str("Password is not filled")))

        if self.can_login(email, password):
            uid = self.get_uid(email)
            self.set_secure_cookie("user_id", uid, 2)
            return self.write(errorJSON(code=1000, message=str("/loonblog/articlelist")))
        else:
            return self.write(errorJSON(code=9004, message=str("Username or Password Error")))

    def can_login(self, email, password):
        sql = SQLite()
        cur = sql.get_cursor()
        cur.execute("select password from loon_person where email=?", (email, ))
        try:
            saved_password = cur.fetchone()[0]
            if pwd_context.verify(password, saved_password):
                return True
            else:
                return False
        except TypeError:
            return False
        finally:
            sql.close_db()


class LogoutHandler(SecureHandler):
    @tornado.web.authenticated
    def get(self):
        uid = self.current_user
        self.clear_cookie('user_id')
        self.redirect("/loonblog")

class EditProfileHandler(SecureHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("edit_profile.html", username = self.get_username())

    @tornado.web.authenticated
    def post(self):
        uid = self.current_user;
        updates = []
        args = ()
        new_username = tornado.escape.xhtml_escape(self.get_argument('username', ""))
        old_password = self.get_argument('old-password', "")
        new_pass1 = self.get_argument('new-password', "")
        new_pass2 = self.get_argument('confirm-password', "")

        sql = SQLite()
        cur = sql.get_cursor()

        if new_pass1 != new_pass2: # new password not match, fail
            self.render("edit_profile.html", username = self.get_username())
            return

        if new_username != '': # modify username
            updates.append("username = ?")
            args = args + (new_username, )

        if old_password != '': # modify password:
            if new_pass1 == '': # new password empty, fail
                self.render("edit_profile.html", username = self.get_username())
                return
            else:
                cur.execute("select password from loon_person where uid = ?", (uid, ))
                try:
                    saved_password = cur.fetchone()[0]
                except TypeError: # fail
                    self.render("edit_profile.html", username = self.get_username())
                    return
                if not pwd_context.verify(old_password, saved_password):
                    # old password incorrect, fail
                    self.render("edit_profile.html",
                            username = self.get_username())
                    return

                # success
                hashed_pass = pwd_context.encrypt(new_pass1)
                updates.append("password = ?")
                args = args + (hashed_pass, )

        if len(updates) == 0: # no entry to be updated
            self.render("edit_profile.html", username = self.get_username())
            return

        query = "update loon_person set "+", ".join(updates)+" where uid = ?"
        cur.execute(query, args + (uid, ))
        sql.commit()
        sql.close_db()

        self.render("edit_profile.html", username = self.get_username())

