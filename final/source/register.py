import tornado.web
import tornado.escape
import sqlite3

from passlib.apps import custom_app_context as pwd_context
from pyvalidate import validate, ValidationException

from base_sqlite import *
from base_secure import *
from apiutil import *

class RegisterHandler(SecureHandler):
    def get(self):
        self.render("register.html")

    def post(self):
        username = tornado.escape.xhtml_escape(self.get_argument("username"))
        email = self.get_argument("email")
        pass1 = self.get_argument("password")
        pass2 = self.get_argument("confirm-password")

        try:
            self.check_params(username=username, email=email, password=pass1, confirm_password=pass2)
        except ValidationException as ex:
            return self.write(errorJSON(code=9001, message=str(ex.message)))

        if pass1 != pass2: # password equal
            return self.write(errorJSON(code=9002, message=str("Passwords don't match")))

        if self.insert_user_success(username, email, pass1):
            self.set_secure_cookie("user_id", self.get_uid(email), 2)
            return self.write(errorJSON(code=1000, message=str("/loonblog/articlelist")))
        else: # email existed
            return self.write(errorJSON(code=9003, message=str("The email has been used")))

    def insert_user_success(self, username, email, raw_pass):
        hashed_pass = pwd_context.encrypt(raw_pass)
        sql = SQLite()
        try:
            cur = sql.get_cursor()

            query = "insert into loon_person(username, email, password) values(?, ?, ?)" 
            cur.execute(query, (username, email, hashed_pass))
            sql.commit()
            return True
        except sqlite3.IntegrityError:
            sql.rollback()
            return False
        finally:
            sql.close_db()
    
    @validate(requires=['username', 'email','password', 'confirm_password'],
            types={'username':str, 'email':str, 'password':str, 'confirm_password':str},
            values={'email':'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', 'phone':'^\d*$'})
    def check_params(self, username, email, password, confirm_password):
        return


class DeleteUserHandler(SecureHandler):
    @tornado.web.authenticated
    def get(self):
        sql = SQLite()
        cur = sql.get_cursor()

        uid = self.current_user
        self.clear_cookie('user_id')
        cur.execute("delete from loon_person where uid=?", (uid, ))
        sql.commit()
        sql.close_db()
        self.redirect("/loonblog")

