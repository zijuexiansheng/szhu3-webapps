import tornado.web
import tornado.escape
import os
import re
import time
import json

from base_sqlite import *
from apiutil import *

dirname = os.path.dirname(__file__)

class SearchAllHandler(tornado.web.RequestHandler):
    def post(self):
        search_content = tornado.escape.xhtml_escape(self.get_argument("search", ""))
        search_option = tornado.escape.xhtml_escape(self.get_argument("search_option", ""))
        sm_aid = self.get_argument("sm_aid", False)
        if sm_aid:
            try:
                sm_aid = int(sm_aid)
                sm_aid = ("loon_article.aid < %d and" % sm_aid)
            except ValueError:
                return self.write(errorJSON(code=7001, message=str("sm_aid should be an integer")))
        else:
            sm_aid = ""

        if not search_content:
            if sm_aid == "":
                self.render('searchall_result.html',
                        checked = search_option,
                        search_content = search_content,
                        res = [])
                return
            else:
                return self.write(errorJSON(code=7002, message=str("search content missing")))
            

        sql = SQLite()
        cur = sql.get_cursor()

        if search_option == 'Tag':
            res = self.search_Tag(search_content, sm_aid, cur)
        elif search_option == 'Title':
            res = self.search_Title(search_content, sm_aid, cur)
        elif search_option == 'Author':
            res = self.search_Author(search_content, sm_aid, cur)
        elif search_option == 'Year':
            res = self.search_Year(search_content, sm_aid, cur)
        elif search_option == 'Content':
            res = self.search_Content(search_content, sm_aid, cur)
        else:
            res = []
            search_option = 'Tag'
        sql.close_db()
        
        if sm_aid == "":
            self.render('searchall_result.html',
                checked = search_option,
                search_content = search_content,
                res = res)
        else:
            if res:
                global dirname
                loader = tornado.template.Loader(os.path.join(dirname, "templates"))
                return self.write(json.dumps({
                            "code": 2000,
                            "message": loader.load("partial_search_result.html").generate(res = res),
                            "sm_aid": res[-1][0]
                        }))
            else:
                return self.write(errorJSON(code=2001, message=str("No more articles")))

    def search_Tag(self, content, sm_aid, cur):
        condition = []
        for each in content.split():
            condition.append("tag_name like '%%%s%%'" % each)
        condition = ' or '.join(condition)
        query = """select distinct loon_article.aid, title, publish_date, abstract
                    from loon_article, loon_tag_article
                    where loon_article.aid = loon_tag_article.aid and %s ( %s )
                    order by publish_date desc limit 20""" % (sm_aid, condition)
        cur.execute(query)
        return cur.fetchall()

    def search_Title(self, content, sm_aid, cur):
        condition = '%' + '%'.join(content.split()) +'%'
        query = """select distinct aid, title, publish_date, abstract
                    from loon_article
                    where %s title like '%s'
                    order by publish_date desc limit 20""" % (sm_aid, condition)
        cur.execute(query)
        return cur.fetchall()

    def search_Author(self, content, sm_aid, cur):
        condition = []
        for each in content.split():
            condition.append("username like '%%%s%%'" % each)
        condition = ' or '.join(condition)
        query = """select distinct aid, title, publish_date, abstract
                    from loon_person, loon_article
                    where loon_person.uid = loon_article.uid 
                    and %s (%s)
                    order by publish_date desc limit 20""" % (sm_aid, condition)
        cur.execute(query)
        return cur.fetchall()

    def search_Year(self, content, sm_aid, cur):
        condition = []
        for each in re.findall("[0-9]+", content):
            condition.append("publish_year=%s" % each)
        condition = ' or '.join(condition)

        if condition == '':
            return []
        query = """select distinct aid, title, publish_date, abstract
                    from loon_article
                    where %s %s
                    order by publish_date desc limit 20""" % (sm_aid, condition)
        cur.execute(query)
        return cur.fetchall()

    def search_Content(self, content, sm_aid, cur):
        condition = '%' + '%'.join(content.split()) +'%' 
        query = """select distinct aid, title, publish_date, abstract
                    from loon_article
                    where %s content like '%s'
                    order by publish_date desc limit 20""" % (sm_aid, condition)
        cur.execute(query)
        return cur.fetchall()

class ViewAllArtivleHandler(tornado.web.RequestHandler):
    def get(self, aid):
        aid = int(aid)
        sql = SQLite()
        cur = sql.get_cursor()
        cur.execute("select aid, title, content from loon_article where aid=?", (aid, ))

        res = cur.fetchone()
        sql.close_db()
        self.render("viewall_article.html",
            res = res)

