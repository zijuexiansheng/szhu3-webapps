import tornado.web

from base_sqlite import *

class SecureHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user_id")
    
    def get_uid(self, email):
        sql = SQLite()
        cur = sql.get_cursor()
        cur.execute("select uid from loon_person where email=?", (email, ))
        uid = str(cur.fetchone()[0])
        sql.close_db()
        return uid
    
    def get_username(self):
        sql = SQLite()
        cur = sql.get_cursor()
        cur.execute("select username from loon_person where uid=?", (self.current_user, ))
        try:
            username = cur.fetchone()[0]
        except TypeError:
            self.redirect("/loonblog/logout")
        finally:
            sql.close_db()
        #print "In get_username():: username = [%s]" % username
        return username

