import os
import time
import tornado.web
import tornado.escape
import json
import re

from apiutil import *
from base_secure import *
from base_sqlite import *

dirname = os.path.dirname(__file__)

class ViewArticleHandler(SecureHandler):
    @tornado.web.authenticated
    def get(self, aid):
        aid = int(aid)
        sql = SQLite()
        cur = sql.get_cursor()
        cur.execute("select aid, title, content from loon_article where aid=?", (aid, ))

        res = cur.fetchone()

        sql.close_db()
        self.render("view_article.html",
                username = self.get_username(),
                res = res)

class ArticlelistHandler(SecureHandler):
    @tornado.web.authenticated
    def get(self):
        sql = SQLite()
        cur = sql.get_cursor()

        cur.execute("select aid, title, publish_date, abstract from loon_article where uid = ? order by aid desc limit 20", (self.current_user, ))
        res = cur.fetchall()

        sql.close_db()
        self.render("articlelist.html", 
                username = self.get_username(),
                res = res)

    @tornado.web.authenticated
    def post(self):
        sm_aid = self.get_argument("sm_aid", False);
        if not sm_aid:
            return self.write(errorJSON(code=8001, message=str("smallest_aid missing")));

        sql = SQLite()
        cur = sql.get_cursor()
        try:
            sm_aid = int(sm_aid)
            cur.execute("select aid, title, publish_date, abstract from loon_article where uid = ? and aid < ? order by aid desc limit 20", (self.current_user, sm_aid))
            res = cur.fetchall()
        except ValueError:
            return self.write(errorJSON(code=8002, message=str("sm_aid should be an integer")))

        if res:
            global dirname
            loader = tornado.template.Loader(os.path.join(dirname, "templates"))
            return self.write(json.dumps({"code": 2000,
                            "message": loader.load("partial_articlelist.html").generate(res = res),
                            "sm_aid": res[-1][0]
                        }))
        else:
            return self.write(errorJSON(code=2001, message=str("No more articles")))
            

class NewArticleHandler(SecureHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("newarticle.html", username = self.get_username())

    @tornado.web.authenticated
    def post(self):
        title = tornado.escape.xhtml_escape(self.get_argument("title", ""))
        content = self.get_argument("content", "");
        tags = self.get_argument("tags", "").split();

        sql = SQLite()
        cur = sql.get_cursor()

        aid = self.save_article(title, content, self.current_user, cur)
        self.save_tag(tags, cur)
        self.save_tag_article(tags, aid, cur)
        sql.commit()
        sql.close_db()

        self.redirect("/loonblog/view-%s-article" % aid)

    def save_tag_article(self, tags, aid, cur):
        for tag in tags:
            cur.execute("insert into loon_tag_article values(?, ?)", (tag, aid))

    def save_tag(self, tags, cur):
        for tag in tags:
            try:
                cur.execute("insert into loon_tag values(?)", (tag, ))
            except sqlite3.IntegrityError:
                pass

    def save_article(self, title, content, uid, cur):
        publish_date = int(time.time())
        publish_year = int(time.ctime(publish_date).split()[-1]);
        abstract = content[:256]

        query = '''insert into loon_article(title, 
                content, 
                publish_year, 
                publish_date,
                abstract,
                uid) values(?, ?, ?, ?, ?, ?)'''
        cur.execute(query, (title,
                    content,
                    publish_year,
                    publish_date,
                    abstract,
                    uid))
        cur.execute("select max(aid) from loon_article")
        aid = cur.fetchone()[0]
        #print "aid =", aid
        return aid

class EditArticleHandler(SecureHandler):
    @tornado.web.authenticated
    def get(self, aid):
        sql = SQLite()
        cur = sql.get_cursor()

        aid = int(aid)
        cur.execute("select aid, title, content from loon_article where aid=? and uid=?", (aid, self.current_user))
        try:
            res = cur.fetchone()
        except TypeError:
            res = None

        if res:
            self.set_secure_cookie("article_id", str(aid), 1)
            cur.execute("select tag_name from loon_tag_article where aid = ?", (aid, ))
            tags = ' '.join([each[0] for each in cur.fetchall()])
        else:
            self.set_secure_cookie("article_id", str(-1), 1)
            tags = None

        sql.close_db()

        self.render("edit_article.html",
                username = self.get_username(),
                res = res,
                tags = tags)

    @tornado.web.authenticated
    def update(self, aid):
        self.do_update_article(aid)
        return self.write(errorJSON(code=2000, message=str("success")))

    @tornado.web.authenticated
    def post(self, aid):
        self.do_update_article(aid)
        self.redirect("/loonblog/view-%s-article" % aid)

    def do_update_article(self, aid):
        if int(aid) != int(self.get_secure_cookie("article_id")): # wrong aid
            self.redirect("/loonblog/logout")
            return

        title = tornado.escape.xhtml_escape(self.get_argument("title", ""))
        content = self.get_argument("content", "")
        tags = self.get_argument("tags", "").split()

        sql = SQLite()
        cur = sql.get_cursor()
        self.update_article(title, content, aid, cur)
        self.save_tag(tags, cur)
        self.update_tag_article(tags, aid, cur)
        sql.commit()
        sql.close_db()

    def update_article(self, title, content, aid, cur):
        query = '''update loon_article
                    set title = ?,
                    content = ?,
                    abstract = ?
                    where aid = ?'''
        cur.execute(query, (title, 
                    content,
                    content[:256], 
                    aid))

    def save_tag(self, tags, cur):
        for tag in tags:
            try:
                cur.execute("insert into loon_tag values(?)", (tag, ))
            except sqlite3.IntegrityError:
                pass

    def update_tag_article(self, tags, aid, cur):
        cur.execute("delete from loon_tag_article where aid = ?", (aid, ))
        for tag in tags:
            cur.execute("insert into loon_tag_article values(?, ?)", (tag, aid))

class DeleteArticleHandler(SecureHandler):
    @tornado.web.authenticated
    def delete(self, aid):
        self.do_delete_article(aid)
        return self.write(errorJSON(code=2000, message="success"))

    @tornado.web.authenticated
    def get(self, aid):
        self.do_delete_article(aid)
        self.redirect("/loonblog/articlelist")

    def do_delete_article(self, aid):
        sql = SQLite()
        cur =sql.get_cursor()
        cur.execute("delete from loon_article where aid = ? and uid = ?", 
                (aid, self.current_user))
        sql.commit()
        sql.close_db()

class SearchHandler(SecureHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("search_article.html", 
                username = self.get_username(),
                checked = 'Tag',
                search_content = None,
                res = None)

    @tornado.web.authenticated
    def post(self):
        search_content = tornado.escape.xhtml_escape(self.get_argument("search", ""))
        search_option = tornado.escape.xhtml_escape(self.get_argument("search_option", ""))
        sm_aid = self.get_argument("sm_aid", False)
        if sm_aid:
            try:
                sm_aid = int(sm_aid)
                sm_aid = ("loon_article.aid < %d and" % sm_aid)
            except ValueError:
                return self.write(errorJSON(code=7001, message=str("sm_aid should be an integer")))
        else:
            sm_aid = ""

        if not search_content:
            if sm_aid == "":
                self.render('search_article.html',
                        username = self.get_username(),
                        checked = search_option,
                        search_content = search_content,
                        res = [])
                return
            else:
                return self.write(errorJSON(code=7002, message=str("search content missing")))
            
        uid = int(self.current_user)
        sql = SQLite()
        cur = sql.get_cursor()

        if search_option == 'Tag':
            res = self.search_Tag(search_content, uid, sm_aid, cur)
        elif search_option == 'Title':
            res = self.search_Title(search_content, uid, sm_aid, cur)
        elif search_option == 'Year':
            res = self.search_Year(search_content, uid, sm_aid, cur)
        elif search_option == 'Content':
            res = self.search_Content(search_content, uid, sm_aid, cur)
        else:
            res = []
            search_option = 'Tag'
        sql.close_db()

        if sm_aid == "":
            self.render('search_article.html',
                    username = self.get_username(),
                    checked = search_option,
                    search_content = search_content,
                    res = res)
        else:
            if res:
                global dirname
                loader = tornado.template.Loader(os.path.join(dirname, "templates"))
                return self.write(json.dumps({
                                "code": 2000,
                                "message": loader.load("partial_search_result.html").generate(res = res),
                                "sm_aid": res[-1][0]
                            }))
            else:
                return self.write(errorJSON(code=2001, message=str("No more articles")))

    def search_Tag(self, content, uid, sm_aid, cur):
        condition = []
        for each in content.split():
            condition.append("tag_name like '%%%s%%'" % each)
        condition = ' or '.join(condition)
        query = """select distinct loon_article.aid, title, publish_date, abstract
                    from loon_article, loon_tag_article
                    where loon_article.aid = loon_tag_article.aid
                        and %s uid=? 
                        and ( %s ) 
                    order by publish_date desc limit 20""" % (sm_aid, condition)
        cur.execute(query, (uid, ))
        return cur.fetchall()

    def search_Title(self, content, uid, sm_aid, cur):
        condition = '%' + '%'.join(content.split()) + '%'
        query = """select distinct aid, title, publish_date, abstract
                from loon_article
                where uid = ? and %s title like '%s'
                order by publish_date desc limit 20""" % (sm_aid, condition)
        cur.execute(query, (uid, ))
        return cur.fetchall()

    def search_Year(self, content, uid, sm_aid, cur):
        condition = []
        for each in re.findall("[0-9]+", content):
            condition.append("publish_year=%s" % each)
        condition = ' or '.join(condition)

        if condition == '':
            return []
        query = """select distinct aid, title, publish_date, abstract
                    from loon_article
                    where ( %s ) and %s uid = ? 
                    order by publish_date desc limit 20""" % (condition, sm_aid)
        cur.execute(query, (uid, ))
        return cur.fetchall()

    def search_Content(self, content, uid, sm_aid, cur):
        condition = '%' + '%'.join(content.split()) + '%'
        query = """select distinct aid, title, publish_date, abstract
                    from loon_article
                    where uid = ? and %s content like '%s'
                    order by publish_date desc limit 20""" % (sm_aid, condition)
        cur.execute(query, (uid, ))
        return cur.fetchall()

class PicturesHandler(SecureHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("pictures.html",
                 username = self.get_username(),
                 pics = self.get_pics(),
                 info_field = None)

    @tornado.web.authenticated
    def post(self):
        try:
            upload_pic = self.request.files['upload_pic'][0]
        except KeyError:
            self.render("pictures.html",
                username=self.get_username(),
                pics = self.get_pics(),
                info_field = False)
            return
        fname = str(self.current_user) + "_" + str(time.time())
        f_type = upload_pic['filename'].split('.')[-1]
        if f_type != upload_pic['filename']:
            fname = fname + '.' + f_type

        # read and save the file
        sql = SQLite()
        cur = sql.get_cursor()
        cur.execute("insert into loon_pictures(fname) values(?)", (fname, ))

        global dirname
        fout = open(os.path.join(dirname, "static/pictures/"+fname), "wb")
        fout.write(upload_pic["body"])
        fout.close()

        sql.commit()

        self.render("pictures.html",
            username = self.get_username(),
            pics = self.get_pics(),
            info_field = ("![image](/static/pictures/%s)" % fname))

    def get_pics(self):
        sql = SQLite()
        cur = sql.get_cursor()
        cur.execute("select fname from loon_pictures order by pid desc")
        ret = [each[0] for each in cur.fetchall()]
        sql.close_db()
        return ret

