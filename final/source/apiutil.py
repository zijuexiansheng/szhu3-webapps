from tornado.escape import json_encode
from types import IntType, StringType

def errorJSON(code,message):
    ''' Return an error object
    Error object is of the form:
    {  'code' : error code (integer),
       'message' : error message (string)}'''
    assert type(code) is IntType, "code needs to be an integer: %r" % code
    assert type(message) is StringType, "message needs to be a string: %r" % message
    # create error object
    error={'code' : code,
           'message' : message}
    return json_encode(error)

