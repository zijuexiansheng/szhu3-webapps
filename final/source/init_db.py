import sqlite3

loon_person = '''create table loon_person(
    uid integer primary key autoincrement,
    username char(50),
    email char(50) unique,
    password char(256)
);'''

loon_article = '''create table loon_article(
    aid integer primary key autoincrement,
    title char(1024),
    content varchar(65536),
    publish_year integer,
    publish_date integer,
    abstract char(256),
    uid integer references loon_person(uid) on delete cascade
);'''

loon_tag = '''create table loon_tag(
    tag_name char(30) primary key
);'''

loon_tag_article='''create table loon_tag_article(
    tag_name char(30) references loon_tag(tag_name) on delete cascade,
    aid integer references loon_article(aid) on delete cascade,
    primary key(tag_name, aid)
);'''

loon_pictures='''create table loon_pictures(
    pid integer primary key autoincrement,
    fname char(68));'''

table_list = [loon_person, 
            loon_article, 
            loon_tag,
            loon_tag_article,
            loon_pictures,
            ]

if __name__ == '__main__':
    conn = sqlite3.connect("LoonBlog.db")
    cur = conn.cursor()

    for each in table_list:
        cur.execute(each)
