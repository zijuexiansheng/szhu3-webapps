import cherrypy
import mysql.connector as MySQL
from mysql.connector import Error
import logging
import json

from menu_itemsid import MenuItemsId

class SubSectionsId(object):
    ''' Handles resource /restuarant/{restId}/menus/{menuId}/sections/{sectionId}'''

    exposed = True
    def __init__(self, db_user, db_passwd, db_host, db_name, env):
        self.miid = MenuItemsId(db_user, db_passwd, db_host, db_name, env)
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host
        self.env = env

    def getTextFromDB(self, subsectionId):
        try:
            conn = MySQL.connect(
                    user = self.db_user,
                    password = self.db_passwd,
                    host = self.db_host,
                    database = self.db_name)
            cur = conn.cursor()

            stmt = "select section_textId, subsectionId, section_text_text from section_text where subsectionId = %(subsectionId)s"
            cur.execute(stmt, {'subsectionId': subsectionId})
            return cur.fetchall()
        except Error as e:
            logging.error(e)
            raise

    def getMenuItemFromDB(self, subsectionId):
        try:
            conn = MySQL.connect(
                    user = self.db_user,
                    password = self.db_passwd,
                    host = self.db_host,
                    database = self.db_name)
            cur = conn.cursor()

            stmt = "select menu_itemId, subsectionId, name, description, price from menu_item where subsectionId = %(subsectionId)s"
            cur.execute(stmt, {'subsectionId': subsectionId})
            return cur.fetchall()
        except Error as e:
            logging.error(e)
            raise

    def GET(self, restId, menuId, sectionId, subsectionId):
        ''' return info on restaurants/{restId}/menus/{menuId} '''
        print ">>> SubSectionsId GET"
        output_format = cherrypy.lib.cptools.accept(['application/json'])
        return json.dumps( [self.getTextFromDB(subsectionId),
                            self.getMenuItemFromDB(subsectionId),
                            restId, 
                            menuId,
                            sectionId], 
                        encoding="utf-8" )

    def PUT(self, restId, menuId, sectionId, subsectionId, **kargs):
        return "lol"

    def DELETE(self, restId, menuId, sectionId, subsectionId):
        return "lol"

    def OPTIONS(self, restId, menuId, sectionId, subsectionId):
        return "lol"
        



