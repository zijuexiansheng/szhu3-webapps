import cherrypy
import mysql.connector as MySQL
from mysql.connector import Error
import logging
import json

from menusid import MenusId

class Menus(object):
    ''' Handles resource  /restaurant/{restId}/menus
        Allowed methods: GET, PUT, DELETE, OPTIONS '''
    exposed = True

    def __init__(self, db_user, db_passwd, db_host, db_name, env):
        self.mid = MenusId(db_user, db_passwd, db_host, db_name, env)

        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host
        self.env = env

    def getMenusFromDB(self, restId):
        try:
            conn = MySQL.connect(
                    user = self.db_user,
                    password = self.db_passwd,
                    host = self.db_host,
                    database = self.db_name)
            cur = conn.cursor()

            stmt = "select menuId, restId, menu_name from menus where restId = %(restId)s"
            cur.execute(stmt, {'restId': restId})
            return cur.fetchall()
        except Error as e:
            logging.error(e)
            raise

    def GET(self, restId):
        ''' return info on restaurants/{restId}/menus '''
        print ">>> Menus GET"
        output_format = cherrypy.lib.cptools.accept(['application/json'])
        
        ret_menus = self.getMenusFromDB(restId)
        return json.dumps(ret_menus, encoding='utf-8')

    def PUT(self, restId, **kargs):
        return "lol"

    def DELETE(self, restId):
        return "lol"

    def OPTIONS(self, restId):
        return "lol"
        
