import cherrypy
import mysql.connector as MySQL
from mysql.connector import Error
import logging
import json

from menus import Menus

class RestaurantId(object):
    ''' Handles resource /restaurant/{id} 
        Allowed methods: GET, PUT, DELETE, OPTIONS'''
    exposed = True

    def __init__(self, db_user, db_passwd, db_host, db_name, env):
        self.rest_menus = Menus(db_user, db_passwd, db_host, db_name, env)
        # print ">>> RestaurantId has attribute rest_menus"
        self.general_info = None
        self.open_hours = None
        self.categories = None
        self.cuisines = None
        self.menus = None
        self.similar_venus = None

        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host
        self.env = env


    def getDataFromDB(self, restId):
        try:
            conn = MySQL.connect(
                    user = self.db_user,
                    password = self.db_passwd,
                    host = self.db_host,
                    database = self.db_name)
            cur = conn.cursor()
            # general info
            stmt = "select name, street_address, locality, region, postal_code from restaurants where restId = %(restId)s"
            cur.execute(stmt, {'restId': restId})
            self.general_info = cur.fetchone()
            if not self.general_info:
                return
            # open hours
            stmt = "select day, open, close from hours where restId = %(restId)s"
            cur.execute(stmt, {'restId': restId})
            self.open_hours = {'M': [],
                                'T': [],
                                'W': [],
                                'TH': [],
                                'F': [],
                                'S': [],
                                'SU': []}
            for (day, open_time, close_time) in cur.fetchall():
                self.open_hours[day].append(str(open_time)+'-'+str(close_time))
            # categories
            stmt = "select name from categories where restId = %(restId)s"
            cur.execute(stmt, {'restId': restId})
            self.categories = [each[0] for each in cur.fetchall()]
            # cuisines
            stmt = "select name from cuisines where restId = %(restId)s"
            cur.execute(stmt, {'restId': restId})
            self.cuisines = [each[0] for each in cur.fetchall()]
            # menus
            stmt = "select menuId, restId, menu_name from menus where restId = %(restId)s"
            cur.execute(stmt, {'restId': restId})
            self.menus = cur.fetchall()
            # similar venus
            stmt = "select similar_to from similar_venus where restId = %(restId)s"
            cur.execute(stmt, {'restId': restId})
            self.similar_venus = []
            for each in cur.fetchall():
                stmt_name = "select name from restaurants where restId = %(restId)s"
                cur.execute(stmt_name, {'restId': each[0]})
                tmp_name = cur.fetchone()
                if tmp_name:
                    self.similar_venus.append((each[0], tmp_name[0]))
        except Error as e:
            logging.error(e)
            raise
            

    def GET(self, restId):
        ''' Return information on restaurant restId '''
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
        self.getDataFromDB(restId)
        if output_format == 'text/html':
            return self.env.get_template('restaurantid.html').render(
                        restId = restId,
                        general_info = self.general_info,
                        open_hours = self.open_hours,
                        categories = self.categories,
                        cuisines = self.cuisines,
                        menus = self.menus,
                        similar_venus = self.similar_venus
                    )
        else:
            return json.dumps({
                                'restId': restId,
                                'general_info': self.general_info,
                                'open_hours': self.open_hours,
                                'categories': self.categories,
                                'cuisines': self.cuisines,
                                'menus': self.menus,
                                'similar_venus': self.similar_venus
                            }, 
                        encoding='utf-8')
        

    def PUT(self, restID, **kwargs):
        ''' Update restaurant with restID'''
        result = "PUT /restaurants/{id=%s}      ...     RestaurantID.PUT\n" % restID
        result += "PUT /restaurants body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update restaurant
        # Prepare response
        return result

    def DELETE(self, restID):
        ''' Delete restaurant with restID'''
        #Validate restID
        #Delete restaurant
        #Prepare response
        return "DELETE /restaurants/{id=%s}   ...   RestaurantID.DELETE" % restID

    def OPTIONS(self, restID):
        ''' Allows GET, PUT, DELETE, OPTIONS '''
        #Prepare response
        return "<p>/restaurants/{id} allows GET, PUT, DELETE, and OPTIONS</p>"

