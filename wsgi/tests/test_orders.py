import requests
s = requests.Session()
host = "http://localhost:8080"

def one_get_json(h, req):
    global s
    s.headers.update({'Accept': 'application/json'})
    print ">>> GET " + req
    r = s.get(h + req)
    print r.status_code
    if r.status_code == requests.codes.ok:
        print r.json()
    print

def one_post_text(h, req, data):
    global s
    s.headers.update({'Accept': 'text/html'})
    print ">>> POST " + req
    r = s.post(h+req, data=data)
    print r.status_code
    print r.text
    print

one_get_json(host, '/orders/1')
one_post_text(host, '/orders/options/37', {'userId': 1})
one_post_text(host, '/orders/menu-item/1145', {'userId': 1})
one_get_json(host, '/orders/1')
