function handle_section(thisObj, event){
        event.preventDefault();
        var href = thisObj.attr("href");
        var section_id = "#section-" +href.substr(href.lastIndexOf('/') + 1);

        if($(section_id).hasClass("panel-body"))
        {
            $(section_id).toggle("slow");
        }
        else
        {
            $.ajax({
                type: 'GET',
                url: href,
                dataType: 'json'
            }).done(function(data){
                $(section_id).toggle();
                $(section_id).addClass("panel-body");
                var restId = data[1];
                var menuId = data[2]
                var n = data[0].length;
                if(n)
                {
                    for(var i=0; i<n; ++i)
                    {
                        var tmp_href = "/restaurants/" + restId + "/menus/" + data[2] + "/sections/" + data[0][i][1] + "/subsections/" + data[0][i][0];
                        var tmp_subsection = "subsection-" + data[0][i][0];
                        $(section_id).append(
                                $("<div></div>").addClass("panel panel-success")
                                    .html(
                                        $("<div></div>").addClass("panel-heading")
                                            .html(
                                                $("<a></a>").addClass("subsection")
                                                    .attr('href', tmp_href)
                                                    .html(
                                                        $("<span></span>").addClass("caret")
                                                    )
                                                    .append(data[0][i][2])
                                                    .append($("<span/>").addClass("label label-default pull-right").append("Subsection"))
                                            )
                                    )
                                    .append($("<div></div>").attr('id', tmp_subsection))
                            );
                    }
                }
                else
                {
                    $(section_id).text("empty!!!");
                }

                $(section_id).toggle("slow");

            }).fail(function(){
                var $alert = (
                  $('<div>')
                    .text('Whoops! Something went wrong.')
                    .addClass('alert')
                    .addClass('alert-danger')
                    .addClass('alert-dismissible')
                    .attr('role', 'alert')
                    .prepend(
                      $('<button>')
                        .attr('type', 'button')
                        .addClass('close')
                        .attr('data-dismiss', 'alert')
                        .html('&times;')
                    )
                    .hide()
                );
                /* Add the alert to the alert container. */
                $('#alerts').append($alert);
                /* Slide the alert into view with an animation. */
                $alert.slideDown();
            });
        }
    }


