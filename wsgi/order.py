import sys
import os.path
import threading
import cherrypy
import os
import math
import json
from collections import OrderedDict
import mysql.connector as MySQL
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
import logging
from config import conf
import getpass

""" 2 kinds of orders:
* /order/menu-item/{menu_itemId}
    * data: userId={userId}
    * In this case, there's no options
* /order/options/{optionId}
    * data: userId={userId}
    * we need to verify whether this is an `OPTION_ADD` or `OPTION_CHOOSE`
    * we need to find the corresponding `menu_item`
    * for `OPTION_ADD`: we need to add both the `menu_item` and the `option` into the cart
"""

class MenuItem(object):
    exposed = True
    def __init__(self, db_user, db_passwd, db_host, db_name, env):
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host
        self.env = env
    
    def get_price(self, menu_itemId):
        try:
            conn = MySQL.connect(
                    user=self.db_user,
                    password = self.db_passwd,
                    host=self.db_host,
                    database=self.db_name)
            cur = conn.cursor()
            stmt = "select price from menu_item where menu_itemId = %(menu_itemId)s limit 1"
            cur.execute(stmt, {"menu_itemId": menu_itemId})
            price = cur.fetchone()
            if price == None:
                return {"Error": None}
            price = price[0]
            try:
                price = float(price)
                return price
            except ValueError:
                if price == 'Market':
                    return {"Error": 'Market'}
                elif price == 'n/a':
                    return {"Error": 'n/a'}
                else:
                    price = price.split()
                    if price[0] == '+':
                        return float(price[1])
                    else:
                        return float(price[0])
            except TypeError:
                return {"Error": None}
        except Error as e:
            logging.error(e)
            raise

        

    def add_item(self, menu_itemId, userId, price):
        try:
            conn = MySQL.connect(
                    user=self.db_user,
                    password = self.db_passwd,
                    host=self.db_host,
                    database=self.db_name)
            cur = conn.cursor()
            stmt = "select orderId from orders where userId = %(userId)s and state = 'N' limit 1"
            cur.execute(stmt, {"userId": userId})
            orderId = cur.fetchall()
            if not orderId:
                # start a new order
                stmt = "insert into orders(userId, state, total_cost, total_cnt, last_activity) values(%(userId)s, 'N', %(total_cost)s, %(total_cnt)s, now())"
                cur.execute(stmt, {"userId": userId,
                            "total_cost": price,
                            "total_cnt": 1,
                        })
                cur.execute("select LAST_INSERT_ID()")
                orderId = cur.fetchone()[0]
                stmt = "insert into order_item(orderId, menu_itemId, cnt, total_price) values(%(orderId)s, %(menu_itemId)s, 1, %(price)s)"
                cur.execute(stmt, {"orderId": orderId,
                                "menu_itemId": menu_itemId,
                                "price": price
                        })
                conn.commit()
            else:
                orderId = orderId[0][0]
                stmt = "insert into order_item(orderId, menu_itemId, cnt, total_price) values(%(orderId)s, %(menu_itemId)s, 1, %(price)s)"
                cur.execute(stmt, {"orderId": orderId,
                                "menu_itemId": menu_itemId,
                                "price": price
                        })
                stmt = "update orders set total_cost = total_cost + %(price)s, total_cnt = total_cnt + 1, last_activity = now() where orderId = %(orderId)s"
                cur.execute(stmt, {"price": price, "orderId": orderId})
                conn.commit()
        except Error as e:
            logging.error(e)
            raise

    def POST(self, menu_itemId, **kwargs):
        print "MenuItem.POST"
        userId = kwargs.get('userId', None)
        if not userId:
            return "No userId"
        price = self.get_price(menu_itemId)
        try:
            price = float(price)
            self.add_item(menu_itemId, userId, price)
            return "succeed"
        except TypeError:
            return "No price for this item"
        

class Options(object):
    exposed = True
    def __init__(self, db_user, db_passwd, db_host, db_name, env):
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host
        self.env = env

    def process_price(self, price):
        if price == None:
            return {"Error": None}
        try:
            price = float(price)
            return price
        except ValueError:
            if price == 'Market':
                return {"Error": 'Market'}
            elif price == 'n/a':
                return {"Error": 'n/a'}
            else:
                price = price.split()
                if price[0] == '+':
                    return float(price[1])
                else:
                    return float(price[0])
        except TypeError:
            return {"Error": None}

    def get_option_info(self, optionId):
        try:
            conn = MySQL.connect(
                    user=self.db_user,
                    password = self.db_passwd,
                    host=self.db_host,
                    database=self.db_name)
            cur = conn.cursor()
            ret = {"Error": None}
            stmt = """select options.price, menu_item.price, menu_item.menu_itemId, optionId, option_type
                        from options, option_group, menu_item
                        where optionId = %(optionId)s and
                            option_group.option_groupId = options.option_groupId and
                            option_group.menu_itemId = menu_item.menu_itemId
                        limit 1"""
            cur.execute(stmt, {"optionId": optionId})
            data = cur.fetchall()
        except Error as e:
            logging.error(e)
            raise
        if not data:
            return {"Error": "No info"}
        else:
            data = data[0]
            p1 = self.process_price(data[0])
            p2 = self.process_price(data[1])
            try:
                ret['p1'] = float(p1)
            except TypeError:
                if p1['Error'] == None:
                    ret['p1'] = None
                else:
                    return {"Error": "price format error"}
            try:
                ret['p2'] = float(p2)
            except TypeError:
                if p2['Error'] == None:
                    ret['p2'] = None
                else:
                    return {"Error": "price format error"}
            ret['menu_itemId'] = data[2]
            ret['optionId'] = data[3]
            ret['option_type'] = data[4]
            return ret

    def process_option_add(self, userId, info):
        if info['p1'] == None or info['p2'] == None:
            return "Lack of price info"
        try:
            conn = MySQL.connect(
                    user=self.db_user,
                    password = self.db_passwd,
                    host=self.db_host,
                    database=self.db_name)
            cur = conn.cursor()
            stmt = "select orderId from orders where userId = %(userId)s and state = 'N' limit 1"
            cur.execute(stmt, {"userId": userId})
            orderId = cur.fetchall()
            if not orderId:
                # new order
                stmt = "insert into orders(userId, state, total_cost, total_cnt, last_activity) values(%(userId)s, 'N', %(total_cost)s, %(total_cnt)s, now())"
                cur.execute(stmt, {'userId': userId,
                                'total_cost': info['p1'] + info['p2'],
                                'total_cnt': 1})
                cur.execute("select LAST_INSERT_ID()")
                orderId = cur.fetchone()[0]
            else:
                # old order
                orderId = orderId[0][0]
                stmt = "update orders set total_cost = total_cost + %(pp)s, total_cnt = total_cnt + 1, last_activity = now() where orderId = %(orderId)s"
                cur.execute(stmt, {"pp": info['p1'] + info['p2'],
                                "orderId": orderId})
            stmt = "insert into order_item(orderId, menu_itemId, cnt, total_price) values(%(orderId)s, %(menu_itemId)s, 1, %(total_price)s)"
            cur.execute(stmt, {"orderId": orderId,
                            "menu_itemId": info['menu_itemId'],
                            "total_price": info['p2']})
            cur.execute("select LAST_INSERT_ID()")
            order_itemId = cur.fetchone()[0]

            stmt = "insert into order_option(order_itemId, optionId, cnt, add_price) values(%(order_itemId)s, %(optionId)s, 1, %(add_price)s)"
            cur.execute(stmt, {"order_itemId": order_itemId,
                            "optionId": info['optionId'],
                            "add_price": info['p1']})
            conn.commit()
            return "success"
        except Error as e:
            logging.error(e)
            raise

    def process_option_choose(self, userId, info):
        info['p1'] = 0
        return self.process_option_add(userId, info)

    def POST(self, optionId, **kwargs):
        print "Options.POST"
        userId = kwargs.get('userId', None)
        if not userId:
            return "No userId"
        option_info = self.get_option_info(optionId)
        if option_info['Error']:
            return option_info['Error']
        else:
            if option_info['option_type'] == 'OPTION_ADD':
                return self.process_option_add(userId, option_info)
            elif option_info['option_type'] == 'OPTION_CHOOSE':
                return self.process_option_choose(userId, option_info)
            else:
                return "unknown option type"



class OrderItems(object):
    exposed = True
    dirname = os.path.abspath( os.path.dirname(__file__) )
    env = Environment(loader = FileSystemLoader(os.path.join(dirname, 'templates')))

    def __init__(self, db_user="test", db_passwd="mypass", db_host="127.0.0.1", db_name="restaurant"):
        self.order_menu_item = MenuItem(db_user, db_passwd, db_host, db_name, self.env)
        self.order_options = Options(db_user, db_passwd, db_host, db_name, self.env)
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host

    def _cp_dispatch(self, vpath):
        print "OrderItems._cp_dispatch with vpath: %s \n" % vpath
        len_vpath = len(vpath)
        if len_vpath == 1: # /order/{userId}
            cherrypy.request.params['userId'] = vpath.pop(0)
            return self
        if len_vpath == 2:
            choice = vpath.pop(0)
            if choice == "menu-item": # /orders/menu-item/{menu_itemId}
                cherrypy.request.params['menu_itemId'] = vpath.pop(0)
                return self.order_menu_item
            elif choice == "options": # /orders/options/{optionId}
                cherrypy.request.params['optionId'] = vpath.pop(0)
                return self.order_options
        return vpath

    def getOrdersFromDB(self, userId):
        try:
            conn = MySQL.connect(
                    user=self.db_user,
                    password = self.db_passwd,
                    host=self.db_host,
                    database=self.db_name)
            cur = conn.cursor()
            # get order: orderId, total_cost, total_cnt, last_activity
            stmt = """select orderId, total_cost, total_cnt, last_activity from orders where userId = %(userId)s"""
            cur.execute(stmt, {"userId": userId})
            ret = []
            for each_order in cur.fetchall():
                one_order = {'orderId': each_order[0],
                            'total_cost': float(each_order[1]),
                            'total_cnt': each_order[2],
                            'last_activity': each_order[3].strftime('%Y-%m-%d %H:%M:%S'),
                            'order_item': []}
                # get order item: order_itemId, total_price, cnt, menu_item.name
                stmt = """select order_itemId, total_price, cnt, menu_item.name
                        from order_item, menu_item
                        where order_item.menu_itemId = menu_item.menu_itemId and orderId = %(orderId)s"""
                cur.execute(stmt, {"orderId": one_order['orderId']})
                for each_order_item in cur.fetchall():
                    one_order_item = {"total_price": float(each_order_item[1]),
                                "cnt": each_order_item[2],
                                "name": each_order_item[3],
                                'order_option': []}
                    # get order option: add_price, cnt, options.name
                    order_itemId = each_order_item[0]
                    stmt = """ select add_price, cnt, options.name
                                from order_option, options
                                where order_option.optionId = options.optionId and order_itemId = %(order_itemId)s
                            """
                    cur.execute(stmt, {"order_itemId": order_itemId})
                    for each_order_option in cur.fetchall():
                        one_order_option = {'add_price': float(each_order_option[0]),
                                        'cnt': each_order_option[1],
                                        'name': each_order_option[2]}
                        one_order_item['order_option'].append( one_order_option )
                    one_order['order_item'].append( one_order_item )
                ret.append(one_order)
            return ret
        except Error as e:
            logging.error(e)
            raise

    def GET(self, userId):
        print "OrderItems GET"
        orders = self.getOrdersFromDB(userId)
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
        if output_format == 'text/html':
            return self.env.get_template("orders.html").render(orders = orders)
        else:
            return json.dumps(orders, encoding='utf-8')
        

    def POST(self, **kwargs):
        return "OrderItems POST"

    def PUT(self):
        return "OrderItems PUT"

    def OPTIONS(self):
        return "<p>/orders/ allows GET, POST, PUT</p>"

class StaticAssets(object):
    pass

if __name__ == '__main__':
    # passwd = getpass.getpass("Input database password")
    passwd = 'M1rt9n+Turn5r'
    cherrypy.tree.mount(OrderItems(db_user = 'root', db_passwd = passwd), '/orders',{
            '/': { 'request.dispatch': cherrypy.dispatch.MethodDispatcher() }
        })
    cherrypy.tree.mount(StaticAssets(), '/', {
            '/': {
                'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
                },
            '/static': {
                'tools.staticdir.on': True,
                'tools.staticdir.dir': 'static'
                }
        })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    sys.stdout = sys.stderr
    application = cherrypy.Application(OrderItems(), None, conf)



