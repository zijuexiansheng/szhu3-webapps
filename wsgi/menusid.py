import cherrypy
import mysql.connector as MySQL
from mysql.connector import Error
import logging
import json

from sectionsid import SectionsId

class MenusId(object):
    ''' Handles resource /restaurant/{restId}/menus/{menuId} '''

    exposed = True

    def __init__(self, db_user, db_passwd, db_host, db_name, env):
        self.sectionsid = SectionsId(db_user, db_passwd, db_host, db_name, env)
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host
        self.env = env

    def getSectionsFromDB(self, menuId):
        try:
            conn = MySQL.connect(
                    user = self.db_user,
                    password = self.db_passwd,
                    host = self.db_host,
                    database = self.db_name)
            cur = conn.cursor()

            stmt = "select sectionId, menuId, section_name from sections where menuId = %(menuId)s"
            cur.execute(stmt, {'menuId': menuId})
            return cur.fetchall()
        except Error as e:
            logging.error(e)
            raise

    def GET(self, restId, menuId):
        ''' return info on restaurants/{restId}/menus/{menuId} '''
        print "MenusId GET"
        output_format = cherrypy.lib.cptools.accept(['application/json'])
        return json.dumps( [self.getSectionsFromDB(menuId), restId], 
                        encoding="utf-8" )

    def PUT(self, restId, menuId, **kargs):
        return "lol"

    def DELETE(self, restId, menuId):
        return "lol"

    def OPTIONS(self, restId, menuId):
        return "lol"
        
