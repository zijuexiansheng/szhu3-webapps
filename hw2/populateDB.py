import mysql.connector as MySQL
import json
from decimal import *
import sys

db_user = 'test'
db_host = '127.0.0.1'
db_name = 'restaurant'
db_passwd = 'mypass'

if len(sys.argv) == 3:
    db_user = sys.argv[1]
    db_passwd = sys.argv[2]

conn = MySQL.connect(user = db_user, password = db_passwd,
                    host=db_host, database = db_name,
                    charset='utf8mb4', use_unicode=True)
cur = conn.cursor()

inputFile = open('restaurantData.json', 'r')
restaurantDict = json.load(inputFile)
inputFile.close()

fout = open("price.txt", "w")

def get_last_insert_id(cur):
    cur.execute("select LAST_INSERT_ID()")
    return cur.fetchone()[0]

for restId, restaurant in restaurantDict.iteritems():
    stmt = """insert into restaurants values(
                    %(restId)s, 
                    %(name)s,
                    %(website_url)s,
                    %(has_menu)s,
                    %(resource_uri)s,
                    %(facebook_url)s,
                    %(twitter_id)s,
                    %(phone)s,
                    %(redirected_from)s,
                    %(street_address)s,
                    %(locality)s,
                    %(region)s,
                    %(postal_code)s,
                    %(country)s,
                    %(lat)s,
                    %(lng)s)"""
    params = {
        'restId': restId,
        'name': restaurant['name'],
        'website_url': restaurant.get('website_url'),
        'has_menu': restaurant['has_menu'],
        'resource_uri': restaurant.get('resource_uri'),
        'facebook_url': restaurant.get('facebook_url'),
        'twitter_id': restaurant.get('twitter_id'),
        'phone': restaurant['phone'],
        'redirected_from': restaurant.get('redirected_from'),
        'street_address': restaurant['street_address'],
        'locality': restaurant['locality'],
        'region': restaurant['region'],
        'postal_code': restaurant['postal_code'],
        'country': restaurant['country'],
        'lat': restaurant['lat'],
        'lng': restaurant['long'],
        }
    cur.execute(stmt, params)

    # cuisines
    stmt = """insert into cuisines values(%(restId)s, %(name)s)"""
    for name in restaurant['cuisines']:
        params = {"restId": restId,
                "name": name}
        cur.execute(stmt, params)

    # categories
    stmt = """insert into categories values(%(restId)s, %(name)s)"""
    for name in restaurant['categories']:
        params = {"restId": restId,
                "name": name}
        cur.execute(stmt, params)

    # similar_venus
    stmt = """insert into similar_venus values(%(restId)s, %(similar_to)s)"""
    for similar_to in restaurant['similar_venues']:
        params = {"restId": restId,
                "similar_to": similar_to}
        cur.execute(stmt, params)

    # open hours
    stmt = """insert into hours values(
                    %(restId)s,
                    %(day)s,
                    %(open)s,
                    %(close)s
                    )"""
    days = {'Monday': 1,
            'Tuesday': 2,
            'Wednesday': 3,
            'Thursday': 4,
            'Friday': 5,
            'Saturday': 6,
            'Sunday': 7}
    for day, times in restaurant['open_hours'].iteritems():
        for each_time in times:
            tmp_time = each_time.split()
            params = {'restId': restId,
                    'day': days[day],
                    'open': tmp_time[0],
                    'close': tmp_time[-1]}
            cur.execute(stmt, params)

    # menus
    stmt_menus = """insert into menus(restId, menu_name, currency_symbol) values(
                    %(restId)s,
                    %(menu_name)s,
                    %(currency_symbol)s)"""
    for menu in restaurant['menus']:
        params_menus = {"restId": restId,
                    "menu_name": menu['menu_name'],
                    "currency_symbol": menu['currency_symbol']}
        cur.execute(stmt_menus, params_menus)
        menuId = get_last_insert_id(cur)

        # sections
        stmt_sections = """insert into sections(menuId, section_name) values(
                            %(menuId)s,
                            %(section_name)s)"""
        for section in menu['sections']:
            params_sections = {"menuId": menuId,
                            "section_name": section['section_name'].replace(u"\u200B", "")}
            cur.execute(stmt_sections, params_sections)
            sectionId = get_last_insert_id(cur)
            # subsections
            stmt_subsections = """insert into subsections(sectionId, subsection_name)
                                values(%(sectionId)s, %(subsection_name)s)"""
            for subsection in section['subsections']:
                params_subsections = {"sectionId": sectionId,
                                    "subsection_name": subsection['subsection_name'].replace(u"\u200B", "")}
                cur.execute(stmt_subsections, params_subsections)
                subsectionId = get_last_insert_id(cur)
                # contents: section_text or menu_item
                stmt_section_text = """insert into section_text(subsectionId, section_text_text)
                                values(%(subsectionId)s, %(section_text_text)s)"""
                stmt_menu_item = """insert into menu_item(subsectionId, name, description, price)
                                values(%(subsectionId)s, %(name)s, %(description)s, %(price)s)"""
                for content in subsection['contents']:
                    if content['type'] == 'SECTION_TEXT': # section_text
                        params_section_text = {"subsectionId": subsectionId,
                                            "section_text_text": content['text']}
                        cur.execute(stmt_section_text, params_section_text)
                        section_textId = get_last_insert_id(cur)
                    else: # menu_item
                        params_menu_item = {"subsectionId": subsectionId,
                                "name": content['name'],
                                "description": content.get('description'),
                                "price": content.get('price')}
                        fout.write("[menu_item]: [%s]\n" % content.get('price'))
                        cur.execute(stmt_menu_item, params_menu_item)
                        menu_itemId = get_last_insert_id(cur)

                        # option_group
                        stmt_option_group = """insert into option_group(menu_itemId, option_type, option_group_text) values(%(menu_itemId)s, %(option_type)s, %(option_group_text)s)"""
                        for option_group in content.get('option_groups', []):
                            params_menu_item = {"menu_itemId": menu_itemId,
                                            "option_type": option_group['type'],
                                            "option_group_text": option_group['text']}
                            cur.execute(stmt_option_group, params_menu_item)
                            option_groupId = get_last_insert_id(cur)
                            # options
                            stmt_options = """insert into options(option_groupId, name, price) values(%(option_groupId)s, %(name)s, %(price)s)"""
                            for option in option_group['options']:
                                params_options = {"option_groupId": option_groupId,
                                                "name": option['name'],
                                                "price": option.get('price')}
                                fout.write("[options]: [%s]\n" % option.get('price'))
                                cur.execute(stmt_options, params_options)
                                optionId = get_last_insert_id(cur)
    conn.commit()

conn.close()
fout.close()

