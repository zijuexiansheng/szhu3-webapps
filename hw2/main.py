import cherrypy
import sys
import mysql.connector as MySQL
import getpass
import os
from jinja2 import Environment, FileSystemLoader

class StaticFilePath:
    def __init__(self, path = 'static'):
        self.static_url = path

class ExampleApp(object):
    dirname = os.path.dirname(__file__)
    env = Environment(loader = FileSystemLoader(os.path.join(dirname, 'templates')))

    def __init__(self, db_user = "test", db_passwd = "mypass", db_host = "127.0.0.1", db_name = "restaurant"):
        # this is for local test
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_host = db_host
        self.db_name = db_name

    @cherrypy.expose
    def index(self):
        conn = MySQL.connect(user=self.db_user, 
                            password=self.db_passwd, 
                            host=self.db_host, 
                            database=self.db_name)
        cur = conn.cursor()
        cur.execute("select name, website_url, street_address, locality, lat, lng from restaurants")

        from collections import OrderedDict
        res = OrderedDict()
        row = cur.fetchone()
        while row:
            res[row[0]] = (row[1], row[2]+', '+row[3], row[4], row[5])
            row = cur.fetchone()
        res = sorted(res.iteritems(), key = lambda x: x[0]) # sort by name
        html = self.env.get_template("index.html")
        return html.render(restaurants = res,
                            static_url = static_url.static_url)

    @cherrypy.expose
    def showdb(self):
        cnx = mysql.connector.connect(user='test', password='mypass',
                              host='127.0.0.1',
                              database='testdb')
        cursor = cnx.cursor()
        query = ("SELECT firstname,lastname,email FROM Invitations")
        cursor.execute(query)
        info = str()
        print cursor
        for (firstname, lastname, email) in cursor:
           info = info + "Full Name:" + lastname + firstname + "Email: "+email
        return info

if __name__ == "__main__":
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
            },
        '/static':{
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'static'
            }
        }
    static_url = StaticFilePath('static/')
    passwd = getpass.getpass("input database password: ")
    cherrypy.quickstart(ExampleApp(db_user = "root", db_passwd = passwd), '/', conf)
else:
    curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
    conf = {
        '/static':{
            'tools.staticdir.on': True,
            'tools.staticdir.root': curdir,
            'tools.staticdir.dir': 'static'
        }
    }
    static_url = StaticFilePath('/api/static/')
    application = cherrypy.Application(ExampleApp(), None)
    application.merge(conf)

