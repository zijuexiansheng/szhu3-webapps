use restaurant;
drop table if exists order_option;
drop table if exists order_item;
drop table if exists orders;
drop table if exists users;
drop table if exists options;
drop table if exists option_group;
drop table if exists menu_item;
drop table if exists section_text;
drop table if exists subsections;
drop table if exists sections;
drop table if exists menus;
drop table if exists hours;
drop table if exists similar_venus;
drop table if exists categories;
drop table if exists cuisines;
drop table if exists restaurants;

create table restaurants(
    restId char(20) primary key,
    name varchar(45) not null,
    website_url varchar(100),
    has_menu bool not null,
    resource_uri varchar(100),
    facebook_url varchar(100),
    twitter_id varchar(50),
    phone varchar(20) not null,
    redirected_from varchar(50),
    street_address varchar(100) not null,
    locality varchar(45) not null,
    region varchar(20) not null,
    postal_code varchar(10) not null,
    country varchar(50) not null,
    lat decimal(11, 8) not null,
    lng decimal(11, 8) not null
);

create table cuisines(
    restId char(20) not null,
    name varchar(50) not null,
    primary key(restId, name),
    foreign key(restId) references restaurants(restId) on delete cascade
);

create table categories(
    restId char(20) not null,
    name varchar(50) not null,
    primary key(restId, name),
    foreign key(restId) references restaurants(restId) on delete cascade
);

create table similar_venus(
    restId char(20) not null,
    similar_to char(20) not null,
    primary key(restId, similar_to),
    /*foreign key(similar_to) references restaurants(restId) on delete cascade,*/
    foreign key(restId) references restaurants(restId) on delete cascade
);

create table hours(
    restId char(20) not null,
    day enum ('M', 'T', 'W', 'TH', 'F', 'S', 'SU') not null,
    open time not null,
    close time not null,
    primary key(restId, day, open),
    foreign key(restId) references restaurants(restId) on delete cascade
);

create table menus(
    menuId int not null auto_increment primary key,
    restId varchar(20) not null,
    menu_name varchar(50) not null,
    currency_symbol char(1) not null,
    foreign key(restId) references restaurants(restId) on delete cascade
);

create table sections(
    sectionId int not null auto_increment primary key,
    menuId int not null,
    section_name varchar(50) not null,
    foreign key(menuId) references menus(menuId) on delete cascade
);

create table subsections(
    subsectionId int not null auto_increment primary key,
    sectionId int not null,
    subsection_name varchar(50) not null,
    foreign key(sectionId) references sections(sectionId) on delete cascade
);

create table section_text(
    section_textId int not null auto_increment primary key,
    subsectionId int not null,
    section_text_text text not null,
    foreign key(subsectionId) references subsections(subsectionId) on delete cascade
);

create table menu_item(
    menu_itemId int not null auto_increment primary key,
    subsectionId int not null,
    name varchar(200) not null,
    description text,
    price varchar(20),
    foreign key(subsectionId) references subsections(subsectionId) on delete cascade
);

create table option_group(
    option_groupId int not null auto_increment primary key,
    menu_itemId int not null,
    option_type char(13) not null,
    option_group_text varchar(256),
    foreign key(menu_itemId) references menu_item(menu_itemId) on delete cascade
);

create table options(
    optionId int not null auto_increment primary key,
    option_groupId int not null,
    name varchar(50) not null,
    price varchar(20),
    foreign key(option_groupId) references option_group(option_groupId) on delete cascade
);

create table users(
    userId int not null auto_increment primary key,
    email varchar(50) unique,
    name varchar(50) not null,
    passwd varchar(256) not null,
    address varchar(100),
    phone char(20),
    balance decimal(11, 2) not null
);

create table orders(
    orderId int not null auto_increment primary key,
    userId int not null,
    state enum ('C', 'F', 'S', 'N', 'R', 'P') not null, -- Cancelled, Finished, Shipped, Not Paid, Refund, Paid
    total_cost decimal(11, 2) not null,
    total_cnt int not null,
    last_activity datetime not null,
    foreign key(userId) references users(userId) on delete cascade
);

create table order_item(
    order_itemId int not null auto_increment primary key,
    orderId int not null,
    menu_itemId int not null,
    cnt int not null,
    total_price decimal(11, 2) not null,
    foreign key(orderId) references orders(orderId) on delete cascade,
    foreign key(menu_itemId) references menu_item(menu_itemId) on delete cascade
);

create table order_option(
    order_optionId int not null auto_increment primary key,
    order_itemId int not null,
    optionId int not null,
    cnt int not null,
    add_price decimal(11, 2) not null,
    foreign key(order_itemId) references order_item(order_itemId) on delete cascade,
    foreign key(optionId) references options(optionId) on delete cascade
);
