import re

head = """import mysql.connector as MySQL
import sys

db_user = 'test'
db_host = '127.0.0.1'
db_name = 'restaurant'
db_passwd = "mypass"

if len(sys.argv) == 3:
    db_user = sys.argv[1]
    db_passwd = sys.argv[2]

conn = MySQL.connect(user = db_user, password = db_passwd, host = db_host, database = db_name, charset='utf8mb4', use_unicode=True)
cur = conn.cursor()
"""

tail = """
conn.commit()
conn.close()
"""

fin = open("restaurant.sql", "r")
fout = open("createDB.py", "wb")
fout.write(head)

sql = re.split(r'\r?\n\r?\n', fin.read())

# output drop tables
drops = ["    '%s'," % each_drop.strip() for each_drop in sql[0].split(";")[1:] if each_drop.strip()]
drops_output = """
drops = [
%s
    ]
""" % "\n".join(drops)

fout.write(drops_output)

exec_drops = """
for each_drop in drops:
    cur.execute(each_drop)
"""
fout.write(exec_drops)

# output create tables
tables_output = '''
tables = [
"""
%s
"""
    ]
''' % "\"\"\",\n\"\"\"".join(sql[1:])
fout.write(tables_output)

exec_create = """
for each_table in tables:
    cur.execute(each_table)
"""
fout.write(exec_create)

fout.write(tail)
