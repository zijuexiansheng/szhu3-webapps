function handle_menu(thisObj, event){
    event.preventDefault();
    var href = thisObj.attr("href");
    var mid = href.substr(href.lastIndexOf('/') + 1);
    var menu_id = "#menu-" + mid;
    if($(menu_id).hasClass("panel-body"))
    {
        $(menu_id).toggle("slow");
    }
    else
    {
        $.ajax({
            type: 'GET',
            url: href,
            dataType: 'json'
        }).done(function(data){
            $(menu_id).toggle();
            $(menu_id).addClass("panel-body");
            var restId = data[1];
            var n = data[0].length;
            if(n)
            {
                for(var i=0; i<n; ++i)
                {
                    var tmp_href = "/restaurants/" + restId + "/menus/" + data[0][i][1] + "/sections/" + data[0][i][0];
                    var tmp_section = "section-" + data[0][i][0];
                    $(menu_id).append(
                            $("<div></div>").addClass("panel panel-success")
                                .html(
                                    $("<div></div>").addClass("panel-heading")
                                        .html(
                                            $("<a></a>").addClass("section")
                                                .attr('href', tmp_href)
                                                .html(
                                                    $("<span></span>").addClass("caret")
                                                )
                                                .append(data[0][i][2])
                                                .append($("<span/>").addClass("label label-default pull-right").append("Section"))
                                        )
                                )
                                .append($("<div></div>").attr('id', tmp_section))
                        );
                }
            }
            else
            {
                $(menu_id).text("empty!!!");
            }

            $(menu_id).toggle("slow");

        }).fail(function(){
            var $alert = (
              $('<div>')
                .text('Whoops! Something went wrong.')
                .addClass('alert')
                .addClass('alert-danger')
                .addClass('alert-dismissible')
                .attr('role', 'alert')
                .prepend(
                  $('<button>')
                    .attr('type', 'button')
                    .addClass('close')
                    .attr('data-dismiss', 'alert')
                    .html('&times;')
                )
                .hide()
            );
            /* Add the alert to the alert container. */
            $('#alerts').append($alert);
            /* Slide the alert into view with an animation. */
            $alert.slideDown();
        });
    }
}

