$(function()
{
    function isValidEmail(email)
    {
        var re = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
        return re.test(email);
    }

    function isValidPassword(pwd) 
    {
        // at least one number, one lowercase, one uppercase letter, one special symbol
        // at least nine characters
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[?!_#%^&@+-]).{9,}/;
        return re.test(pwd);
    }

    function isValidPhone(phone) {
        // phone must be all digits
        var re = /^\d*$/;
        return re.test(phone);
    }

    function showErrorMsg(msg)
    {
        $.globalMessenger().post({
                message: msg,
                type: "error",
                showCloseButton: "true"
            });
    }

    $('#user-add-form').on('submit', function(event) { // form id
        event.preventDefault();
        var name = $('#user-name-input').val();
        var email = $('#user-email-input').val();
        var pwd = $('#user-password-input').val();
        var confirm_pwd = $("#user-confirm-password-input").val();
        var phone = $('#user-phone-input').val();
        var address = $('#user-address-input').val();

        console.log("before submit form")

        if(name == "")
        {
            showErrorMsg('All fields must be filled. "name" is empty');
            return;
        }

        if(email == "")
        {
            showErrorMsg('All fields must be filled. "email" is empty');
            return;
        }
        
        if(pwd == "")
        {
            showErrorMsg('All fields must be filled. "password" is empty');
            return;
        }

        if(pwd != confirm_pwd)
        {
            showErrorMsg('"password" and "confirm password" don\'t match ');
            return;
        }

        if(phone == "")
        {
            showErrorMsg('All fields must be filled. "phone" is empty');
            return;
        }

        if(address == "")
        {
            showErrorMsg('All fields must be filled. "address" is empty');
            return;
        }

        if(!isValidEmail(email))
        {
            showErrorMsg('Email must be in the correct format');
            return;
        }

        if(!isValidPassword(pwd))
        {
            showErrorMsg('Password has to be 9 or more characters, and contain at least 1 upper case, 1 lower case, 1 number, and 1 symbol.');
            return;
        }

        if(!isValidPhone(phone))
        {
            showErrorMsg("Phone must be all digits.");
            return;
        }

        
        $.ajax({
            type: 'POST',
            url: '/user/registration',
            data: $(this).serialize(),
            dataType: 'json'
        }).done(function(data){
            if(data["errors"].length)
            {
                var error_msg = "";
                for(i in data["errors"])
                    error_msg += data["errors"][i]["code"] + ": " + data["errors"][i]["message"] + '\r\n';
                showErrorMsg(error_msg);
            }
            else
            {
                $.globalMessenger().post({
                        message: "Success! Please login!",
                        type: "success",
                        showCloseButton: "true"
            });
            }
        })
    });
});


