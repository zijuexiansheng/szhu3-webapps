function handle_subsection(thisObj, event){
        console.log("In handle_subsection");
        event.preventDefault();
        var href = thisObj.attr("href");
        var subsection_id = "#subsection-" +href.substr(href.lastIndexOf('/') + 1);

        if($(subsection_id).hasClass("panel-body"))
        {
            $(subsection_id).toggle("slow");
        }
        else
        {
            console.log("fetch data");
            $.ajax({
                type: 'GET',
                url: href,
                dataType: 'json'
            }).done(function(data){
                console.log("subsection data received");
                $(subsection_id).toggle();
                $(subsection_id).addClass("panel-body");
                var restId = data[2];
                var menuId = data[3];
                var sectionId = data[4];
                if(data[0].length)
                {
                    var n = data[0].length;// section_text
                    var tmp_list = $("<ul></ul>").addClass("list-group");
                    for(var i=0; i<n; ++i)
                    {
                        tmp_list.append($("<li></li>").addClass("list-group-item list-group-item-warning")
                                            .append(data[0][i][2])
                            );
                    }
                    $(subsection_id).append(tmp_list);
                }
                
                if(data[1].length)
                {
                    var n = data[1].length;// menu item
                    for(var i=0; i<n; ++i)
                    {
                        var tmp_href = "/restaurants/" + restId + "/menus/" + menuId + "/sections/" + sectionId + '/subsections/' + data[1][i][1] + "/menu_items/" + data[1][i][0];
                        var tmp_menu_item = "menu_item-" + data[1][i][0];
                        var tmp_price = "";
                        var tmp_desc = ""
                        if(data[1][i][4] != null)
                            tmp_price = ": " + data[1][i][4] + "$";
                        if(data[1][i][3] != null)
                            tmp_desc = "<p>" + data[1][i][3] + "</p>";


                        $(subsection_id).append(
                                $("<div></div>").addClass("panel panel-danger")
                                    .html(
                                        $("<div></div>").addClass("panel-heading")
                                            .html(
                                                $("<a></a>").addClass("menu_item")
                                                    .attr('href', tmp_href)
                                                    .html(
                                                        $("<span></span>").addClass("caret")
                                                    )
                                                    .append(data[1][i][2] + tmp_price)
                                                    .append($("<span/>").addClass("label label-default pull-right").append("menu item"))
                                                                    
                                            )
                                            .append(tmp_desc)
                                    )
                                    .append($("<div></div>").attr('id', tmp_menu_item))
                            );
                    }
                }
                if(data[0].length ==0 && data[1].length == 0)
                {
                    $(subsection_id).text("empty!!!");
                }

                $(subsection_id).toggle("slow");

            }).fail(function(){
                var $alert = (
                  $('<div>')
                    .text('Whoops! Something went wrong.')
                    .addClass('alert')
                    .addClass('alert-danger')
                    .addClass('alert-dismissible')
                    .attr('role', 'alert')
                    .prepend(
                      $('<button>')
                        .attr('type', 'button')
                        .addClass('close')
                        .attr('data-dismiss', 'alert')
                        .html('&times;')
                    )
                    .hide()
                );
                /* Add the alert to the alert container. */
                $('#alerts').append($alert);
                /* Slide the alert into view with an animation. */
                $alert.slideDown();
            });
        }
    }



