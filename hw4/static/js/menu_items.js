function handle_menu_item(thisObj, event){
        event.preventDefault();
        var href = thisObj.attr("href");
        var mid = href.substr(href.lastIndexOf('/') + 1);
        var menu_item_id = "#menu_item-" + mid;

        if($(menu_item_id).hasClass("panel-body"))
        {
            $(menu_item_id).toggle("slow");
        }
        else
        {
            $.ajax({
                type: 'GET',
                url: href,
                dataType: 'json'
            }).done(function(data){
                $(menu_item_id).toggle();
                $(menu_item_id).addClass("panel-body");
                var restId = data[2];
                var menuId = data[3];
                var sectionId = data[4];
                var subsectionId = data[5];
                n = data[0].length;
                if(n)
                {
                    for(var i=0; i<n; ++i)
                    {
                        var m = data[1][i].length;

                        var tmp_desc = $("<span/>");
                        if(data[0][i][3])
                            tmp_desc.append(data[0][i][3]+"&nbsp;&nbsp;");

                        tmp_desc.append(option_type);

                        var tmp_panel = $("<div/>").addClass("panel panel-danger")
                                                   .append($("<div/>").addClass("panel-heading").append(tmp_desc));
                        var tmp_list = $("<div/>").addClass("list-group");
                        var option_type = "";
                        if(data[0][i][2] == "OPTION_ADD")
                            option_type = $("<span/>").addClass("badge").append("Addon");
                        else if(data[0][i][2] == "OPTION_CHOOSE")
                                option_type = $("<span/>").addClass("badge").append("Choose");

                        for(var j = 0; j<m; ++j)
                        {
                            var tmp_href = "/restaurants/" + restId + "/menus/" + data[2] + "/sections/" + sectionId + '/subsections/' + subsectionId + "/menu_items/" + data[0][i][1] + "/option_group/" + data[0][i][0] + "/options/" + data[1][i][j][0];
                            var name_price = $("<span/>").append(data[1][i][j][2]);
                            if(data[1][i][j][3] != null)
                                name_price.append(": " + data[1][i][j][3] + "$");

                            var tmp_uid = $("<input type='hidden' name='userId' value=" + $("#userId").text() + ">");
                            var tmp_submit = $("<input class='btn btn-primary' type='submit' value='Add to Cart'>");
                            var tmp_form = $("<form/>").addClass("form-inline")
                                                .attr("method", "POST")
                                                .attr("action", "/orders/options/"+data[1][i][j][0])
                                                .append(tmp_uid)
                                                .append(tmp_submit)
                            tmp_list.append(
                                    $("<a/>").addClass("list-group-item list-group-item-danger")
                                            .attr('href', "#" + tmp_href)
                                            .append(name_price)
                                            .append(tmp_form)
                                );


                        }
                        $(menu_item_id).append( tmp_panel.append(tmp_list) );
                    }
                }
                else
                {
                    var tmp_uid = $("<input type='hidden' name='userId' value="+ $("#userId").text() +">");
                    var tmp_submit = $("<input class='btn btn-primary' type='submit' value='Add to Cart'>")
                    var tmp_form = $("<form/>").addClass("form-inline")
                                            .attr('method', 'POST')
                                            .attr("action", "/orders/menu-item/"+mid)
                                            .append(tmp_uid)
                                            .append(tmp_submit);
                    $(menu_item_id).append(tmp_form);
                }

                $(menu_item_id).toggle("slow");

            }).fail(function(){
                var $alert = (
                  $('<div>')
                    .text('Whoops! Something went wrong.')
                    .addClass('alert')
                    .addClass('alert-danger')
                    .addClass('alert-dismissible')
                    .attr('role', 'alert')
                    .prepend(
                      $('<button>')
                        .attr('type', 'button')
                        .addClass('close')
                        .attr('data-dismiss', 'alert')
                        .html('&times;')
                    )
                    .hide()
                );
                /* Add the alert to the alert container. */
                $('#alerts').append($alert);
                /* Slide the alert into view with an animation. */
                $alert.slideDown();
            });
        }
    }




