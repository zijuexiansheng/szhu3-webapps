''' Utility functions for FeedND API'''
import cherrypy
import json
from types import IntType,StringType

def errorJSON(code,message):
    ''' Return an error object
    Error object is of the form:
    {  'code' : error code (integer),
       'message' : error message (string)}'''
    assert type(code) is IntType, "code needs to be an integer: %r" % code
    assert type(message) is StringType, "message needs to be a string: %r" % message
    # create error object
    error={'code' : code,
           'message' : message}
    errors={'errors':[error]}
    return json.dumps(errors)

def secureheaders():
    headers = cherrypy.response.headers
    headers['X-Frame-Options'] = 'DENY'
    headers['X-XSS-Protection'] = '1; mode=block'
    headers['Content-Security-Policy'] = "default-src='self'"

# set the priority according to your needs if you are hooking something
# else on the 'before_finalize' hook point.
cherrypy.tools.secureheaders = cherrypy.Tool('before_finalize', secureheaders, priority=60)    
