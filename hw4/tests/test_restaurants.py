import requests
s = requests.Session()
host = "http://localhost"

s.headers.update({'Accept': 'application/json'})

def one_get(h, req):
    global s
    print ">>>", req
    r = s.get(h + req)
    print r.status_code
    if r.status_code == requests.codes.ok:
        print r.json()
    print


one_get(host, '/restaurants')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd/menus')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd/menus/22')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd/menus/22/sections')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd/menus/22/sections/145')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd/menus/22/sections/145')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd/menus/22/sections/145/subsections')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd/menus/22/sections/145/subsections/148')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd/menus/22/sections/145/subsections/148/menu_items')
one_get(host, '/restaurants/7009c6cc1083e4c6b3dd/menus/22/sections/145/subsections/148/menu_items/1145')
one_get(host, '/restaurants/00dae7092df41d0f8df2/menus/4/sections/18/subsections/18/menu_items/92')
