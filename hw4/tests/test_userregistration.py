import requests
s = requests.Session()
host = "http://localhost"

def one_post_json(h, req, data):
    global s
    s.headers.update({'Accept': 'application/json'})
    print ">>> POST " + req
    print data
    r = s.post(h+req, data=data)
    print r.status_code
    print r.json()
    print

req = '/user/registration'
data = {}
one_post_json(host, req, data)

data["name"] = "test2"
one_post_json(host, req, data)

data["email"] = "test@nd"
one_post_json(host, req, data)

data["password"] = "AbCd-EfGh123"
one_post_json(host, req, data)

data["confirm_password"] = "1234567"
one_post_json(host, req, data)

data["confirm_password"] = "AbCd-EfGh123"
one_post_json(host, req, data)

data["phone"] = "abcde"
one_post_json(host, req, data)

data["address"] = "lol addr"
one_post_json(host, req, data)

data["phone"] = "12345678"
one_post_json(host, req, data)

data["email"] = "test@nd.edu"
one_post_json(host, req, data)

one_post_json(host, req, data)

data['email'] = 'test20@nd.edu'
one_post_json(host, req, data)
