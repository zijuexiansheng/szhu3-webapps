import cherrypy
import mysql.connector as MySQL
from mysql.connector import Error
import logging
import json

from subsectionsid import SubSectionsId

class SectionsId(object):
    ''' Handles resource /restuarant/{restId}/menus/{menuId}/sections/{sectionId}'''

    exposed = True
    def __init__(self, db_user, db_passwd, db_host, db_name, env):
        self.suid = SubSectionsId(db_user, db_passwd, db_host, db_name, env)
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name
        self.db_host = db_host
        self.env = env

    def getSubSectionsFromDB(self, sectionId):
        try:
            conn = MySQL.connect(
                    user = self.db_user,
                    password = self.db_passwd,
                    host = self.db_host,
                    database = self.db_name)
            cur = conn.cursor()

            stmt = "select subsectionId, sectionId, subsection_name from subsections where sectionId = %(sectionId)s"
            cur.execute(stmt, {'sectionId': sectionId})
            return cur.fetchall()
        except Error as e:
            logging.error(e)
            raise

    def GET(self, restId, menuId, sectionId):
        ''' return info on restaurants/{restId}/menus/{menuId} '''
        print ">>> SectionsId GET"
        output_format = cherrypy.lib.cptools.accept(['application/json'])
        return json.dumps( [self.getSubSectionsFromDB(sectionId), 
                            restId, 
                            menuId], 
                        encoding="utf-8" )

    def PUT(self, restId, menuId, sectionId, **kargs):
        return "lol"

    def DELETE(self, restId, menuId, sectionId):
        return "lol"

    def OPTIONS(self, restId, menuId, sectionId):
        return "lol"
        



