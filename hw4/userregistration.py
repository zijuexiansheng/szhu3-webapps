import apiutil
from apiutil import errorJSON
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import json
import mysql.connector as MySQL
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
import logging
from pyvalidate import validate, ValidationException
from passlib.apps import custom_app_context as pwd_context

class UserRegistration(object):
    ''' Handles resource /users/registration
        Allowed methods: GET, POST, OPTIONS '''
    exposed = True
    env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))


    def GET(self):
        ''' Prepare user registration page '''
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        if output_format == 'text/html':
            return self.env.get_template('userregistration.html').render()

    @validate(requires=['name', 'email', 'password', 'confirm_password', 'phone', 'address'],
              types={'name':unicode, 'email':str, 'password':str, 'confirm_password': str, 'phone':str, 'address':str},
              values={'email':'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', 'phone':'^\d*$' }
            )
    def check_params(self, name, email, password, confirm_password, phone, address):
        return
        #print 'adding user "%s:%s" with email: %s:%s phone: %s:%s password: %s:%s address: %s:%s' % (
        #                name, type(name),
        #                email, type(email),
        #                phone, type(phone),
        #                password, type(password),
        #                address, type(address))


    @cherrypy.tools.json_in(force=False)
    def POST(self, name=None, email=None, password=None, confirm_password=None, phone=None, address=None):
        """add a new user"""
        if not name:
            try:
                name = cherrypy.request.json["name"]
                #print "name received: %s" % name
            except:
                print "name was not received"
                return errorJSON(code=9001, message="Expected text 'name' for user as JSON input")
        if not email:
            try:
                email = cherrypy.request.json["email"]
                #print "email received: %s" % email
            except:
                print "email was not received"
                return errorJSON(code=9001, message="Expected email 'email' for user as JSON input")
        if not password:
            try:
                password = cherrypy.request.json["password"]
                #print "password received: %s" % password
            except:
                print "password was not received"
                return errorJSON(code=9001, message="Expected password 'password' for user as JSON input")
        if not confirm_password:
            try:
                confirm_password = cherrypy.request.json["confirm_password"]
                #print "password received: %s" % confirm_password
            except:
                print "confirm_password was not received"
                return errorJSON(code=9001, message="Expected password 'confirm_password' for user as JSON input")
        if password != confirm_password:
            print "password doesn't match"
            return errorJSON(code=9001, message="'password' doesn't match the 'confirm password'")
        if not phone:
            try:
                phone = cherrypy.request.json["phone"]
                #print "phone received: %s" % phone
            except:
                print "phone was not received"
                return errorJSON(code=9001, message="Expected tel 'phone' for user as JSON input")
        if not address:
            try:
                phone = cherrypy.request.json["address"]
                #print "address received: %s" % address
            except:
                print "address was not received"
                return errorJSON(code=9001, message="Expected address 'address' for user as JSON input")
        

        try:
            self.check_params(name=name, email=email, password=password, confirm_password=confirm_password, phone=phone, address=address)
        except ValidationException as ex:
            print "check_params exception:"
            print ex.message
            return errorJSON(code=9002, message=str(ex.message))

        db = cherrypy.request.app.config['databases'];
        cnx = MySQL.connect(user=db['user'],
                        password = db['passwd'],
                        host= db['host'],
                        database= db['db_name'])
        cursor = cnx.cursor()
        # email is unique, so insert will cause an error, and we catch it!!!
        q="INSERT INTO users (name, email, passwd, address, phone, balance) VALUES (%s, %s, %s, %s, %s, 0)"
        try:
            hashed_pwd = pwd_context.encrypt(password)
            cursor.execute(q, (name, email, hashed_pwd, address, phone))
            #userID=cursor.fetchall()[0][0]
            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert user
            if e.errno == 1062:
                print "User with email %s already exists" % email
                return errorJSON(code=9101, message=("User with email %s already exists")) % email
            else:
                print "mysql error: %s" % e
                return errorJSON(code=9102, message="Failed to add user")
        result = {'name':name, 'email':email, 'password': hashed_pwd, 'phone':phone, 'address': address, 'errors':[]}
        return json.dumps(result)        

    def OPTIONS(self):
        ''' Allows GET, POST, OPTIONS '''
        #Prepare response
        return "<p>/users/registration allows GET, POST, and OPTIONS</p>"

class StaticAssets(object):
    pass

dirname = os.path.abspath(os.path.dirname(__file__))

if __name__ == '__main__':
    cherrypy.tree.mount(StaticAssets(), '/', config=os.path.join(dirname, 'static.conf'))
    cherrypy.tree.mount(UserRegistration(), '/user/registration', config = os.path.join(dirname, 'test_app.conf'))
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    dirname = os.path.abspath( os.path.dirname(__file__) )
    application = cherrypy.Application(UserRegistration(), None, config=os.path.join(dirname, "app.conf"))



